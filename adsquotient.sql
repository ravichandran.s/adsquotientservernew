-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2020 at 11:06 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adsquotient`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(65) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `email`, `created_time`, `updated_time`) VALUES
(1, 'ravi', '$2y$10$1bz5UqV2Xwhr1V.gIs.eUOGHSAjVR1jdMDV/GKdJSZ0oBtDwpAJ3a', NULL, NULL, NULL),
(2, 'kushal', '$2y$10$wdvp3AH084VazVo1NpZu4O4JrZ75sSSeQkz2tBfXIA.LqioBlV3GK', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `all_campaign_data`
--

CREATE TABLE `all_campaign_data` (
  `id` int(11) NOT NULL,
  `aqID` int(11) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `last_updated` varchar(20) NOT NULL,
  `json_data` longtext NOT NULL,
  `benchmarks` longtext NOT NULL,
  `campaign_names` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `benchmark_details`
--

CREATE TABLE `benchmark_details` (
  `id` int(11) NOT NULL,
  `fb_CPL` float DEFAULT NULL,
  `fb_CPC` float DEFAULT NULL,
  `fb_CTR` float DEFAULT NULL,
  `fb_CPTP` float DEFAULT NULL,
  `fb_CPM` float DEFAULT NULL,
  `fb_CPE` float DEFAULT NULL,
  `fb_CPI` float DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `aqsector` varchar(45) NOT NULL,
  `objective` varchar(60) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `benchmark_details`
--

INSERT INTO `benchmark_details` (`id`, `fb_CPL`, `fb_CPC`, `fb_CTR`, `fb_CPTP`, `fb_CPM`, `fb_CPE`, `fb_CPI`, `city`, `aqsector`, `objective`, `last_update`) VALUES
(1, 801.398, 30.26, 0.97, 94.48, 339.18, 0, NULL, 'Chennai', 'real_estate_lessthan_50l', 'lead_generation', '2019-09-11 09:45:42'),
(2, 1043.05, 40.93, 0.66, 120.86, 312.98, 0, NULL, 'Chennai', 'real_estate_50l_1c', 'lead_generation', '2019-09-11 09:45:43'),
(3, 1178.61, 41.55, 0.75, 70.89, 322.34, 0, NULL, 'Chennai', 'real_estate_1c_2c', 'lead_generation', '2019-09-11 09:45:43'),
(4, 883.63, 37.36, 0.85, 222.84, 346.2, 0, NULL, 'Chennai', 'real_estate_greaterthan_2c', 'lead_generation', '2019-09-11 09:45:44'),
(5, 557.01, 69.53, 0.74, 659.47, 463.33, 0, NULL, 'Bangalore', 'real_estate_lessthan_50l', 'lead_generation', '2019-09-11 09:45:44'),
(6, 1299.78, 66.3, 0.74, 111.43, 445.32, 0, NULL, 'Bangalore', 'real_estate_50l_1c', 'lead_generation', '2019-09-11 09:45:45'),
(7, 1610.57, 81.84, 0.58, 268.67, 445.32, 0, NULL, 'Bangalore', 'real_estate_1c_2c', 'lead_generation', '2019-09-11 09:45:46'),
(8, 3181.41, 48.65, 0.88, 45.38, 413.4, 0, NULL, 'Bangalore', 'real_estate_greaterthan_2c', 'lead_generation', '2019-09-11 09:45:46'),
(9, 0, 0, 0, 0, 420.8, 0, NULL, 'Mumbai', 'real_estate_lessthan_50l', 'lead_generation', '2019-09-11 09:45:47'),
(10, 1148.17, 109.11, 0.55, 0, 597.4, 0, NULL, 'Mumbai', 'real_estate_50l_1c', 'lead_generation', '2019-09-11 09:45:48'),
(11, 1328.31, 64.49, 1.11, 205.03, 498.53, 0, NULL, 'Mumbai', 'real_estate_1c_2c', 'lead_generation', '2019-09-11 09:45:48'),
(12, 1237.27, 46.46, 0.55, 0, 255.05, 0, NULL, 'Mumbai', 'real_estate_greaterthan_2c', 'lead_generation', '2019-09-11 09:45:49'),
(13, 0, 10.27, 1.26, 36.02, 69.42, 0, NULL, 'Chennai', 'real_estate_lessthan_50l', 'link_clicks', '2019-09-11 09:56:18'),
(14, 0, 8.56, 1.34, 41.26, 99.48, 0, NULL, 'Chennai', 'real_estate_50l_1c', 'link_clicks', '2019-09-11 09:56:18'),
(15, 0, 4.81, 1.86, 36.02, 63.72, 0, NULL, 'Chennai', 'real_estate_1c_2c', 'link_clicks', '2019-09-11 09:56:18'),
(16, 0, 5.96, 1.58, 36.02, 71.27, 0, NULL, 'Chennai', 'real_estate_greaterthan_2c', 'link_clicks', '2019-09-11 09:56:18'),
(17, 0, 12.195, 0.58, 39.31, 67.19, 0, NULL, 'Bangalore', 'real_estate_lessthan_50l', 'link_clicks', '2019-09-11 09:56:18'),
(18, 0, 29.34, 0.67, 31.57, 76.52, 0, NULL, 'Bangalore', 'real_estate_50l_1c', 'link_clicks', '2019-09-11 09:56:18'),
(19, 0, 7.53, 0.92, 51.57, 69.15, 0, NULL, 'Bangalore', 'real_estate_1c_2c', 'link_clicks', '2019-09-11 09:56:18'),
(20, 0, 7.53, 0.92, 51.57, 69.15, 0, NULL, 'Bangalore', 'real_estate_greaterthan_2c', 'link_clicks', '2019-09-11 09:56:18'),
(21, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'real_estate_lessthan_50l', 'link_clicks', '2019-09-11 09:56:18'),
(22, 0, 40.18, 0.72, 0, 27.2, 0, NULL, 'Mumbai', 'real_estate_50l_1c', 'link_clicks', '2019-09-11 09:56:18'),
(23, 0, 40.18, 0.72, 0, 27.2, 0, NULL, 'Mumbai', 'real_estate_1c_2c', 'link_clicks', '2019-09-11 09:56:18'),
(24, 0, 40.18, 0.72, 0, 27.2, 0, NULL, 'Mumbai', 'real_estate_greaterthan_2c', 'link_clicks', '2019-09-11 09:56:18'),
(25, 0, 227.98, 0.06, 8.16, 122.39, 0.85, NULL, 'Chennai', 'real_estate_lessthan_50l', 'post_engagement', '2019-09-11 09:45:57'),
(26, 0, 0, 0, 0, 120.83, 0.48, NULL, 'Chennai', 'real_estate_50l_1c', 'post_engagement', '2019-09-11 09:45:58'),
(27, 0, 126.08, 0.18, 1.02, 124.95, 0.54, NULL, 'Chennai', 'real_estate_1c_2c', 'post_engagement', '2019-09-11 09:45:59'),
(28, 0, 212.6, 0.06, 2.08, 118, 0.99, NULL, 'Chennai', 'real_estate_greaterthan_2c', 'post_engagement', '2019-09-11 09:45:59'),
(29, 0, 357.38, 0.05, 1.48, 172.27, 0.98, NULL, 'Bangalore', 'real_estate_lessthan_50l', 'post_engagement', '2019-09-11 09:46:00'),
(30, 0, 240, 0.08, 0.54, 163.82, 0.41, NULL, 'Bangalore', 'real_estate_50l_1c', 'post_engagement', '2019-09-11 09:46:01'),
(31, 0, 299.4, 0.03, 0, 105.87, 0.48, NULL, 'Bangalore', 'real_estate_1c_2c', 'post_engagement', '2019-09-11 09:46:01'),
(32, 0, 299.4, 0.03, 0, 105.87, 0.49, NULL, 'Bangalore', 'real_estate_greaterthan_2c', 'post_engagement', '2019-09-11 09:46:02'),
(33, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'real_estate_lessthan_50l', 'post_engagement', '2019-09-11 09:46:03'),
(34, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'real_estate_50l_1c', 'post_engagement', '2019-09-11 09:46:03'),
(35, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'real_estate_1c_2c', 'post_engagement', '2019-09-11 09:46:04'),
(36, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'real_estate_greaterthan_2c', 'post_engagement', '2019-09-11 09:46:05'),
(37, 0, 87.15, 0.17, 0.38, 53.34, 0, NULL, 'Chennai', 'real_estate_lessthan_50l', 'video_views', '2019-09-11 09:46:05'),
(38, 0, 53.87, 0.42, 6.98, 87.42, 0, NULL, 'Chennai', 'real_estate_50l_1c', 'video_views', '2019-09-11 09:46:06'),
(39, 0, 82.3, 0.22, 0.34, 48.37, 0, NULL, 'Chennai', 'real_estate_1c_2c', 'video_views', '2019-09-11 09:46:07'),
(40, 0, 87.15, 0.17, 0.38, 58.34, 0, NULL, 'Chennai', 'real_estate_greaterthan_2c', 'video_views', '2019-09-11 09:46:07'),
(41, 0, 80.62, 0.16, 0.39, 58.94, 0, NULL, 'Bangalore', 'real_estate_lessthan_50l', 'video_views', '2019-09-11 09:46:08'),
(42, 0, 52.1, 0.3, 0.53, 61.18, 0, NULL, 'Bangalore', 'real_estate_50l_1c', 'video_views', '2019-09-11 09:46:09'),
(43, 0, 52.1, 0.3, 0.53, 61.18, 0, NULL, 'Bangalore', 'real_estate_1c_2c', 'video_views', '2019-09-11 09:46:09'),
(44, 0, 66.8, 0.16, 0.53, 63.07, 0, NULL, 'Bangalore', 'real_estate_greaterthan_2c', 'video_views', '2019-09-11 09:46:10'),
(45, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'real_estate_lessthan_50l', 'video_views', '2019-09-11 09:46:11'),
(46, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'real_estate_50l_1c', 'video_views', '2019-09-11 09:46:11'),
(47, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'real_estate_1c_2c', 'video_views', '2019-09-11 09:46:12'),
(48, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'real_estate_greaterthan_2c', 'video_views', '2019-09-11 09:46:13'),
(49, 277.17, 39.6, 0.86, 0, 342.09, 0, NULL, 'Chennai', 'bfsi_mutual_funds', 'lead_generation', '2019-09-11 09:46:13'),
(50, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'bfsi_mutual_funds', 'lead_generation', '2019-09-11 09:46:14'),
(51, 0, 4.91, 0.55, 2.77, 27.01, 0, NULL, 'Chennai', 'bfsi_mutual_funds', 'link_clicks', '2019-09-11 09:56:18'),
(52, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'bfsi_mutual_funds', 'link_clicks', '2019-09-11 09:56:18'),
(54, 0, 241.96, 1.09, 0.38, 69.29, 0.15, NULL, 'Mumbai', 'bfsi_mutual_funds', 'post_engagement', '2019-09-11 09:46:17'),
(55, 0, 173.77, 0.51, 0.61, 37.43, 0, NULL, 'Chennai', 'bfsi_mutual_funds', 'video_views', '2019-09-11 09:46:17'),
(56, 0, 126.78, 1.3, 0.36, 36.89, 0, NULL, 'Mumbai', 'bfsi_mutual_funds', 'video_views', '2019-09-11 09:46:18'),
(57, 292.39, 17.35, 1.03, 65.49, 179.14, 0, NULL, 'Mumbai', 'retail_jewellery', 'lead_generation', '2019-09-11 09:46:19'),
(58, 0, 3.63, 1.54, 9.02, 56.04, 0, NULL, 'Mumbai', 'retail_jewellery', 'link_clicks', '2019-09-11 09:56:18'),
(59, 0, 35.89, 0.21, 4.18, 75.15, 0.66, NULL, 'Mumbai', 'retail_jewellery', 'post_engagement', '2019-09-11 09:46:20'),
(60, 0, 74.24, 1.12, 1.02, 48.44, 0, NULL, 'Mumbai', 'retail_jewellery', 'video_views', '2019-09-11 09:46:21'),
(61, 0, 0, 0, 0, 0, 0, NULL, 'Chennai', 'retail_jewellery', 'lead_generation', '2019-09-11 09:46:21'),
(62, 0, 0, 0, 0, 0, 0, NULL, 'Chennai', 'retail_jewellery', 'link_clicks', '2019-09-11 09:56:18'),
(63, 0, 49.89, 0.11, 0, 57.17, 0.26, NULL, 'Chennai', 'retail_jewellery', 'post_engagement', '2019-09-11 09:46:22'),
(64, 0, 0, 0, 0, 0, 0, NULL, 'Chennai', 'retail_jewellery', 'video_views', '2019-09-11 09:46:23'),
(65, 33.54, 9.92, 0.79, 0, 78, 0, NULL, 'Chennai', 'retail_fashion', 'lead_generation', '2019-09-11 09:46:24'),
(66, 0, 3.98, 0.95, 9.23, 37.78, 0, NULL, 'Chennai', 'retail_fashion', 'link_clicks', '2019-09-11 09:56:18'),
(67, 0, 35.5, 0.16, 1.23, 58.43, 0.34, NULL, 'Chennai', 'retail_fashion', 'post_engagement', '2019-09-11 09:46:25'),
(68, 0, 37.05, 0.1, 0.32, 36.71, 0, NULL, 'Chennai', 'retail_fashion', 'video_views', '2019-09-11 09:46:26'),
(69, 256.46, 31.86, 0.78, 0, 248.07, 0, NULL, 'Bangalore', 'retail_fashion', 'lead_generation', '2019-09-11 09:46:26'),
(70, 0, 4.54, 1.14, 25.89, 51.54, 0, NULL, 'Bangalore', 'retail_fashion', 'link_clicks', '2019-09-11 09:56:18'),
(71, 0, 140.48, 1.71, 3.29, 84.46, 0.38, NULL, 'Bangalore', 'retail_fashion', 'post_engagement', '2019-09-11 09:46:28'),
(72, 0, 120.44, 3.18, 0, 49.23, 0, NULL, 'Bangalore', 'retail_fashion', 'video_views', '2019-09-11 09:46:28'),
(73, 0, 0, 0, 0, 0, 0, NULL, 'Mumbai', 'ecommerce_fashion', 'lead_generation', '2019-09-11 09:46:29'),
(74, 0, 3.12, 2.09, 0, 65.3, 0, NULL, 'Mumbai', 'ecommerce_fashion', 'link_clicks', '2019-09-11 09:56:18'),
(75, 0, 243.88, 0.11, 0, 269.02, 3.68, NULL, 'Mumbai', 'ecommerce_fashion', 'post_engagement', '2019-09-11 09:46:30'),
(76, 0, 8.47, 0.54, 0.47, 46.03, 0, NULL, 'Mumbai', 'ecommerce_fashion', 'video_views', '2019-09-11 09:46:31'),
(77, 26.67, 4.25, 1.82, 0, 77.14, 0, NULL, 'Mumbai', 'fmcg_food', 'lead_generation', '2019-09-11 09:46:32'),
(78, 0, 1.99, 0.68, 13.48, 1.52, 0, NULL, 'Mumbai', 'fmcg_food', 'link_clicks', '2019-09-11 09:56:18'),
(79, 0, 10.81, 0, 1.26, 7.61, 0.1, NULL, 'Mumbai', 'fmcg_food', 'post_engagement', '2019-09-11 09:46:33'),
(80, 0, 12.76, 0.15, 0.05, 19, 0, NULL, 'Mumbai', 'fmcg_food', 'video_views', '2019-09-11 09:46:34'),
(81, 659.45, 27.79, 0.91, 0, 253.42, 0, NULL, 'Chennai', 'healthcare_singlespeciality', 'lead_generation', '2019-09-11 09:46:34'),
(82, 0, 0, 0, 0, 0, 0, NULL, 'Chennai', 'healthcare_singlespeciality', 'link_clicks', '2019-09-11 09:56:18'),
(83, 0, 253.87, 4.18, 1.54, 75.9, 0.46, NULL, 'Chennai', 'healthcare_singlespeciality', 'post_engagement', '2019-09-11 09:46:35'),
(84, 0, 173.77, 2.09, 0.61, 37.43, 0, NULL, 'Chennai', 'healthcare_singlespeciality', 'video_views', '2019-09-11 09:46:36'),
(85, 0, 0, 0, 0, 0, 0, NULL, 'Chennai', 'healthcare_multispeciality', 'link_clicks', '2019-09-11 09:56:18'),
(86, 119.99, 30.14, 0.88, 0, 266.22, 0, NULL, 'Chennai', 'healthcare_multispeciality', 'lead_generation', '2019-09-11 09:46:38'),
(87, 0, 35.83, 0.3, 0, 107.22, 3.08, NULL, 'Chennai', 'healthcare_multispeciality', 'post_engagement', '2019-09-11 09:46:38'),
(88, 0, 0, 0, 0, 0, 0, NULL, 'Chennai', 'healthcare_multispeciality', 'video_views', '2019-09-11 09:46:39'),
(89, 388.7, 39.92, 0.46, 98.11, 178.58, 0, NULL, 'Bangalore', 'healthcare_multispeciality', 'lead_generation', '2019-09-11 09:46:39'),
(90, 0, 6.94, 1.01, 2893.19, 60.77, 0, NULL, 'Bangalore', 'healthcare_multispeciality', 'link_clicks', '2019-09-11 09:56:18'),
(91, 0, 362.32, 5.89, 1.32, 102.38, 0.42, NULL, 'Bangalore', 'healthcare_multispeciality', 'post_engagement', '2019-09-11 09:46:41'),
(92, 0, 98.32, 0.79, 0.49, 48.64, 0, NULL, 'Bangalore', 'healthcare_multispeciality', 'video_views', '2019-09-11 09:46:42'),
(93, 679.2, 49.81, 0.86, 376.97, 407.77, 0, NULL, 'All', 'real_estate_lessthan_50l', 'lead_generation', '2019-09-11 09:46:42'),
(94, 1163.66, 72.11, 0.65, 116.14, 451.9, 0, NULL, 'All', 'real_estate_50l_1c', 'lead_generation', '2019-09-11 09:46:43'),
(95, 1372.49, 62.62, 0.81, 181.53, 422.06, 0, NULL, 'All', 'real_estate_1c_2c', 'lead_generation', '2019-09-11 09:46:43'),
(96, 1767.43, 44.15, 0.76, 134.11, 338.21, 0, NULL, 'All', 'real_estate_greaterthan_2c', 'lead_generation', '2019-09-11 09:46:44'),
(97, 0, 11.23, 0.92, 37.66, 68.3, 0, NULL, 'All', 'real_estate_lessthan_50l', 'link_clicks', '2019-09-11 09:56:18'),
(98, 0, 26.02, 0.91, 36.41, 67.73, 0, NULL, 'All', 'real_estate_50l_1c', 'link_clicks', '2019-09-11 09:56:18'),
(99, 0, 17.5, 1.17, 43.79, 53.35, 0, NULL, 'All', 'real_estate_1c_2c', 'link_clicks', '2019-09-11 09:56:18'),
(100, 0, 17.89, 1.07, 43.79, 55.87, 0, NULL, 'All', 'real_estate_greaterthan_2c', 'link_clicks', '2019-09-11 09:56:18'),
(101, 0, 292.68, 0.06, 4.82, 147.33, 0.91, NULL, 'All', 'real_estate_lessthan_50l', 'post_engagement', '2019-09-11 09:46:48'),
(102, 0, 240, 0.08, 0.54, 142.32, 0.44, NULL, 'All', 'real_estate_50l_1c', 'post_engagement', '2019-09-11 09:46:48'),
(103, 0, 212.74, 0.11, 1.02, 115.41, 0.51, NULL, 'All', 'real_estate_1c_2c', 'post_engagement', '2019-09-11 09:46:49'),
(104, 0, 256, 0.05, 2.08, 111.93, 0.74, NULL, 'All', 'real_estate_greaterthan_2c', 'post_engagement', '2019-09-11 09:46:50'),
(105, 0, 83.88, 0.17, 0.38, 56.14, 0, NULL, 'All', 'real_estate_lessthan_50l', 'video_views', '2019-09-11 09:46:50'),
(106, 0, 52.98, 0.36, 3.75, 74.3, 0, NULL, 'All', 'real_estate_50l_1c', 'video_views', '2019-09-11 09:46:51'),
(107, 0, 67.2, 0.26, 0.43, 54.77, 0, NULL, 'All', 'real_estate_1c_2c', 'video_views', '2019-09-11 09:46:52'),
(108, 0, 76.97, 0.17, 0.45, 60.7, 0, NULL, 'All', 'real_estate_greaterthan_2c', 'video_views', '2019-09-11 09:46:52'),
(109, 277.17, 39.6, 0.86, 0, 342.09, 0, NULL, 'All', 'bfsi_mutual_funds', 'lead_generation', '2019-09-11 09:46:53'),
(110, 0, 4.91, 0.55, 2.77, 27.01, 0, NULL, 'All', 'bfsi_mutual_funds', 'link_clicks', '2019-09-11 09:56:18'),
(111, 0, 865.21, 3.76, 0.61, 73.79, 0.21, NULL, 'All', 'bfsi_mutual_funds', 'post_engagement', '2019-09-11 09:46:55'),
(112, 0, 150.27, 0.91, 0.48, 37.16, 0, NULL, 'All', 'bfsi_mutual_funds', 'video_views', '2019-09-11 09:46:55'),
(113, 292.39, 17.35, 1.03, 65.49, 179.14, 0, NULL, 'All', 'retail_jewellery', 'lead_generation', '2019-09-11 09:46:56'),
(114, 0, 3.63, 1.54, 9.02, 179.14, 0, NULL, 'All', 'retail_jewellery', 'link_clicks', '2019-09-11 09:56:18'),
(115, 0, 42.89, 0.16, 4.18, 66.16, 0.46, NULL, 'All', 'retail_jewellery', 'post_engagement', '2019-09-11 09:46:57'),
(116, 0, 74.24, 1.12, 1.02, 48.44, 0, NULL, 'All', 'retail_jewellery', 'video_views', '2019-09-11 09:46:58'),
(117, 145, 20.89, 0.79, 0, 163.03, 0, NULL, 'All', 'retail_fashion', 'lead_generation', '2019-09-11 09:46:59'),
(118, 0, 4.26, 1.05, 17.56, 44.66, 0, NULL, 'All', 'retail_fashion', 'link_clicks', '2019-09-11 09:56:18'),
(119, 0, 87.99, 0.11, 2.26, 71.44, 0.36, NULL, 'All', 'retail_fashion', 'post_engagement', '2019-09-11 09:47:00'),
(120, 0, 78.74, 1.64, 0.32, 42.97, 0, NULL, 'All', 'retail_fashion', 'video_views', '2019-09-11 09:47:01'),
(121, 0, 0, 0, 0, 0, 0, NULL, 'All', 'ecommerce_fashion', 'lead_generation', '2019-09-11 09:47:01'),
(122, 0, 3.12, 2.09, 0, 65.3, 0, NULL, 'All', 'ecommerce_fashion', 'link_clicks', '2019-09-11 09:56:18'),
(123, 0, 243.88, 0.11, 0, 269.02, 3.68, NULL, 'All', 'ecommerce_fashion', 'post_engagement', '2019-09-11 09:47:03'),
(124, 0, 8.47, 0.54, 0.47, 46.03, 0, NULL, 'All', 'ecommerce_fashion', 'video_views', '2019-09-11 09:47:03'),
(125, 26.67, 4.25, 1.82, 0, 77.14, 0, NULL, 'All', 'fmcg_food', 'lead_generation', '2019-09-11 09:47:04'),
(126, 0, 1.99, 0.68, 13.48, 1.52, 0, NULL, 'All', 'fmcg_food', 'link_clicks', '2019-09-11 09:56:18'),
(127, 0, 10.81, 0, 1.26, 7.61, 0.1, NULL, 'All', 'fmcg_food', 'post_engagement', '2019-09-11 09:47:05'),
(128, 0, 12.76, 0.15, 0.05, 19, 0, NULL, 'All', 'fmcg_food', 'video_views', '2019-09-11 09:47:06'),
(129, 659.45, 27.97, 0.91, 0, 253.42, 0, NULL, 'All', 'healthcare_singlespeciality', 'lead_generation', '2019-09-11 09:47:06'),
(130, 0, 0, 0, 0, 0, 0, NULL, 'All', 'healthcare_singlespeciality', 'link_clicks', '2019-09-11 09:56:18'),
(131, 0, 253.87, 4.18, 1.54, 75.9, 0.46, NULL, 'All', 'healthcare_singlespeciality', 'post_engagement', '2019-09-11 09:47:08'),
(132, 0, 173.77, 2.09, 0.61, 37.43, 0, NULL, 'All', 'healthcare_singlespeciality', 'video_views', '2019-09-11 09:47:08'),
(133, 254.34, 35.03, 0.67, 98.11, 222.4, 0, NULL, 'All', 'healthcare_multispeciality', 'lead_generation', '2019-09-11 09:47:09'),
(134, 0, 6.94, 1.01, 2893.19, 60.77, 0, NULL, 'All', 'healthcare_multispeciality', 'link_clicks', '2019-09-11 09:56:18'),
(135, 0, 199.07, 0.17, 1.32, 104.8, 1.74, NULL, 'All', 'healthcare_multispeciality', 'post_engagement', '2019-09-11 09:47:10'),
(136, 0, 98.32, 0.79, 0.49, 48.64, 0, NULL, 'All', 'healthcare_multispeciality', 'video_views', '2019-09-11 09:47:11'),
(137, 0, 1488.46, 6.42, 0.84, 78.3, 0.27, NULL, 'Chennai', 'bfsi_mutual_funds', 'post_engagement', '2019-09-11 09:54:29'),
(138, 0, 7, 0.88, 0, 65.75, 0, 21.95, 'All', 'app_installs_ecommerce', 'app_installs', '2019-11-04 06:52:19'),
(139, 0, 20.49, 0.6, 0, 122.15, 0, 182.72, 'All', 'app_installs_healthcare', 'app_installs', '2019-11-04 12:15:44'),
(140, 0, 27, 0.46, 0, 110.9, 0, 167, 'All', 'app_installs_transport', 'app_installs', '2019-11-06 06:08:24'),
(141, 0, 6.3, 0.83, 0, 52.6, 0, 26.28, 'All', 'app_installs_b2b', 'app_installs', '2019-11-06 06:08:24');

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE `campaigns` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `campaign_id` text DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `aqcampname` varchar(45) DEFAULT NULL,
  `aqsector` varchar(75) DEFAULT NULL,
  `aqinsights` text DEFAULT NULL,
  `aqcity` varchar(45) DEFAULT NULL,
  `fb_ad_account` text DEFAULT NULL,
  `fb_ad_account_name` varchar(255) DEFAULT NULL,
  `added_time` datetime DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`id`, `client_id`, `campaign_id`, `platform`, `aqcampname`, `aqsector`, `aqinsights`, `aqcity`, `fb_ad_account`, `fb_ad_account_name`, `added_time`, `updated_time`) VALUES
(40, 4, '6142919982297', NULL, 'Lead Gen', 'real_estate_50l_1c', NULL, 'Chennai', 'act_16819025', NULL, '2019-09-03 06:22:04', '2019-09-06 11:23:12'),
(41, 4, '23843903549550008,23843903548470008,23843903545840008,23843903544780008,23843903544140008,23843902661430008,23843902659380008,23843902658060008', NULL, 'Casagrand Test', 'real_estate_lessthan_50l', NULL, 'Chennai', 'act_1803104496402413', 'Feathers Hotel', '2019-09-03 06:25:23', '2019-12-02 07:50:50'),
(42, 4, '23843151690140684,23843085745690684,23843080799980684,23843026893880684,23843025042160684', NULL, 'Shriram Properties', 'real_estate_50l_1c', NULL, 'Chennai', 'act_647082735456768', 'SB Brigade', '2019-09-03 06:25:44', '2019-12-02 07:58:22'),
(45, 4, NULL, NULL, 'malabar_gold', 'single_speciality', NULL, 'Chennai', NULL, NULL, '2019-09-05 05:54:12', '2019-09-05 05:54:12'),
(55, 11, NULL, NULL, 'testing camp', 'real_estate_50l_1c', NULL, 'Chennai', 'act_647082735456768', NULL, '2019-09-06 11:09:25', '2019-09-06 11:12:55'),
(56, 11, NULL, NULL, 'July campaign', 'real_estate_lessthan_50l', NULL, 'Chennai', 'act_522039044627805', NULL, '2019-09-06 11:25:16', '2019-09-06 11:28:18'),
(57, 12, '6143012234097,6143006145897,6142919982297,6142918864297,6142836395297', NULL, 'July campaign', 'real_estate_50l_1c', NULL, 'Chennai', 'act_123476804416739', NULL, '2019-09-06 11:31:22', '2019-09-06 13:28:58'),
(64, 14, '23843160336340684,23843151690140684,23843151180510684', NULL, 'July campaign', 'real_estate_50l_1c', NULL, 'Chennai', 'act_647082735456768', NULL, '2019-09-08 14:56:40', '2019-09-08 15:27:02'),
(71, 4, '23843728931510548,23843664275180548,23843661842660548,23843618692390548,23843353834420548,23843327169850548,23843302892450548,23843301835730548,23843289026940548,23843260612040548,23843201727520548,23843201188100548,23843132304960548,23843108861730548,23843098256880548,23843067298010548,23843056813980548,23843044218600548,23843044192210548,23843027595950548,23843027346420548,23843016789210548,23843015513920548,23843015439410548,23843015048640548', NULL, 'Major bulider ECR', 'real_estate_50l_1c', NULL, 'Chennai', 'act_329919047566786', 'Casagrand ECR', '2019-09-09 07:08:44', '2019-11-15 11:28:28'),
(72, 5, '6146139251297,6146045544497,6146006864497,6145816227897,6145815915497,6145815175697,6145814924297,6145814014897,6145813439297,6145810841297,6145810274897,6145809493497,6145809262697,6145808766097,6145808551497,6145807968097,6145806726697,6145805674297,6145803022297,6145802765097,6145802466697,6145802293697,6145801237697,6145801025697,6145800753897,6145800710897,6145797707097', NULL, 'July campaign', 'real_estate_50l_1c', NULL, 'Chennai', 'act_1497399147190652', 'Malabar Gold and Diamonds', '2019-09-09 14:13:22', '2019-10-06 04:53:15'),
(73, 8, '23843728931510548,23843664275180548,23843661842660548,23843618692390548,23843353834420548,23843327169850548,23843302892450548,23843301835730548,23843289026940548,23843260612040548,23843239424830548,23843212788920548,23843203571030548,23843201727520548,23843201188100548,23843132304960548,23843108861730548,23843098256880548,23843067298010548,23843056813980548,23843044218600548,23843027595950548,23843027346420548,23843016789210548,23843015513920548,23843015439410548,23843015048640548,23843014873730548,23843014728990548,23843014722440548,23843011705440548,23843007624250548,23842995799100548,23842993062650548', NULL, 'Casagrand ECR', 'real_estate_50l_1c', NULL, 'Chennai', 'act_329919047566786', 'Casagrand ECR', '2019-09-09 14:19:11', '2019-09-09 14:22:01'),
(74, 7, '6128712032258,6128711975658,6128711940058,6128375231258,6128371265658,6128127574258,6128127540858,6128127426858,6128037527258,6128037084658,6120687438058,6120386272058,6120272028058,6120178454058,6120079663458,6120078790258,6120077455058,6119865310458,6119701812858,6119417416258,6119417028458', NULL, 'Sundaram', 'bfsi_mutual_funds', NULL, 'Chennai', 'act_110172152506718', 'Sundaram', '2019-09-10 09:52:17', '2019-09-11 17:56:45'),
(75, 5, '6143885527897,6143884900297,6143884496897,6143884384697,6143884194297,6143884013297,6143882544897,6143882340497,6143877554897,6143876605497,6143800570097,6143800328897,6143799488497,6143799116497,6143798907897', NULL, 'Madan test dataset', 'real_estate_greaterthan_2c', NULL, 'Bangalore', 'act_1497399147190652', 'Malabar Gold and Diamonds', '2019-09-14 10:00:21', '2019-09-14 10:02:56'),
(76, 5, '23843667116570449,23843663858340449,23843663814770449,23843652414220449,23843648586070449,23843575853590449', NULL, 'Demo dataset', 'real_estate_1c_2c', NULL, 'Chennai', 'act_277103256174822', 'casagrand Brand 2018', '2019-09-25 07:24:16', '2019-11-04 13:33:21'),
(78, 18, NULL, NULL, 'arju_test_dataset', 'real_estate_1c_2c', NULL, 'Chennai', NULL, NULL, '2019-09-30 11:43:26', '2019-09-30 11:43:26'),
(80, 5, '6148085492897,6147951341297,6147505796897,6147182661097,6147073768697,6147052421097,6146636302497', NULL, 'Arju camp', 'ecommerce_fashion', NULL, 'Bangalore', 'act_1497399147190652', 'Malabar Gold and Diamonds', '2019-10-22 09:45:36', '2019-10-22 09:46:36'),
(81, 5, '6085600198439,6083329189639,6075030382839,6072168501839,6065703665639,6063454380239', NULL, 'July campaign', 'real_estate_50l_1c', NULL, 'Chennai', 'act_522039044627805', 'Casagrand  PropCare', '2019-10-25 11:19:26', '2019-10-25 11:20:48'),
(82, 8, '23843542505890735', NULL, 'Casagrand Asta', 'real_estate_50l_1c', NULL, 'Chennai', 'act_244704076189486', 'Casagrand Asta', '2019-10-31 09:57:20', '2019-10-31 10:00:28'),
(83, 5, '23843954923540241', NULL, 'For app install', 'app_installs', NULL, 'Chennai', 'act_1284106758408807', 'Openup - KhataBook Ad Account', '2019-11-01 09:58:20', '2019-11-01 10:05:27'),
(84, 5, '23843954923540241', NULL, 'Khoot app', 'app_installs', NULL, 'Chennai', 'act_1284106758408807', 'Openup - KhataBook Ad Account', '2019-11-01 10:12:02', '2019-11-01 10:48:49'),
(85, 19, NULL, NULL, 'Test', 'multi_speciality', NULL, 'Chennai', NULL, NULL, '2019-11-04 07:39:25', '2019-11-04 07:39:25'),
(88, 21, '23843667943170449,23843667116570449', NULL, 'Demo Campaign', 'real_estate_50l_1c', NULL, 'Chennai', 'act_277103256174822', 'casagrand Brand 2018', '2019-11-04 13:21:50', '2019-11-04 13:23:22'),
(90, 8, NULL, NULL, 'Mfine', 'app_installs_healthcare', NULL, 'All cities', NULL, NULL, '2019-11-04 14:14:42', '2019-11-04 14:14:42'),
(91, 5, '23843966791260241,23843966765890241,23843961542900241', NULL, 'For app install2', 'app_installs_transport', NULL, 'All cities', 'act_1284106758408807', 'Openup - KhataBook Ad Account', '2019-11-05 05:11:17', '2019-11-05 05:11:59'),
(92, 8, NULL, NULL, 'App install', 'app_installs_b2b', NULL, 'All cities', NULL, NULL, '2019-11-05 05:28:32', '2019-11-05 05:28:32'),
(93, 5, NULL, NULL, 'For app install3', 'app_installs_ecommerce', NULL, 'All cities', NULL, NULL, '2019-11-05 05:29:32', '2019-11-05 05:29:32'),
(94, 5, '23843966791260241,23843966765890241,23843961542900241', NULL, 'Mfine', 'app_installs_healthcare', NULL, 'All cities', 'act_1284106758408807', 'Openup - KhataBook Ad Account', '2019-11-05 05:33:07', '2019-11-05 08:09:00'),
(95, 5, '23843943285230241,23843940927650241,23843940828540241,23843940817540241', NULL, 'KhataBook', 'app_installs_b2b', NULL, 'All cities', 'act_1284106758408807', 'Openup - KhataBook Ad Account', '2019-11-05 09:10:45', '2019-11-05 09:11:23'),
(96, 20, '23843667099820449,23843663858340449', NULL, 'Demo Dataset', 'real_estate_50l_1c', NULL, 'Chennai', 'act_277103256174822', 'casagrand Brand 2018', '2019-11-14 05:25:49', '2019-12-03 14:03:01'),
(97, 22, '', NULL, 'Demo Campaign', 'real_estate_50l_1c', NULL, 'Chennai', 'act_277103256174822', 'casagrand Brand 2018', '2019-11-15 13:08:07', '2019-11-15 13:13:36'),
(103, 5, '23843997060530460,23843850979660460,23843829606080460,23843805077690460,23843805053350460,23843794995830460,23843794837610460', NULL, 'Ranga Temp', 'real_estate_lessthan_50l', NULL, 'Chennai', 'act_111341422929495', 'Mfine Novocura', '2019-12-02 09:39:29', '2019-12-05 08:55:43'),
(104, 27, NULL, NULL, 'Test', 'real_estate_lessthan_50l', NULL, 'All cities', NULL, NULL, '2019-12-03 09:39:46', '2019-12-03 09:39:46'),
(106, 28, '23843667943170449,23843667116570449,23843667099820449,23843663858340449,23843663814770449', NULL, 'Demo Campaign', 'real_estate_lessthan_50l', NULL, 'All cities', 'act_277103256174822', 'casagrand Brand 2018', '2019-12-03 09:49:28', '2019-12-03 09:57:56'),
(108, 23, '23844125138800241', NULL, 'AQ Dataset 1', 'real_estate_lessthan_50l', NULL, 'All cities', 'act_1284106758408807', 'Openup - KhataBook Ad Account', '2019-12-03 10:49:31', '2019-12-03 10:51:42'),
(110, 26, '', NULL, 'Demo', 'real_estate_lessthan_50l', NULL, 'Chennai', 'act_1284106758408807', 'Openup - KhataBook Ad Account', '2019-12-03 10:56:39', '2019-12-03 10:57:31'),
(111, 26, '23844125138800241', NULL, '1234', 'multi_speciality', NULL, 'Chennai', 'act_1284106758408807', 'Openup - KhataBook Ad Account', '2019-12-05 05:28:34', '2019-12-05 05:33:14'),
(112, 26, '23844110970950241', NULL, '4345', 'real_estate_lessthan_50l', NULL, 'All cities', 'act_1284106758408807', 'Openup - KhataBook Ad Account', '2019-12-05 06:33:54', '2019-12-05 06:38:30'),
(118, 4, '6152684406770,6151985738570,6145304533370,6145294892570', NULL, 'Test', 'real_estate_50l_1c', NULL, 'Chennai', 'act_16819025', 'Social Beat Ad Account', '2019-12-19 12:19:24', '2019-12-19 11:19:24'),
(119, 4, '6152684406770,6151985738570,6145304533370,6145294892570', NULL, 'Arju DS Test From New UI', 'real_estate_1c_2c', NULL, 'Chennai', 'act_16819025', 'Social Beat Ad Account', '2019-12-19 12:23:20', '2019-12-19 11:23:20'),
(124, 4, '23843151690140684,23843151180510684,23843151019900684,23843151144320684,23842763545100684,23842699800080684', NULL, 'Test Dataset', 'real_estate_50l_1c', NULL, 'Chennai', 'act_647082735456768', 'SB Brigade', '2019-12-19 13:58:14', '2019-12-19 12:58:14'),
(125, 4, '6151985738570,6152684406770,6145304533370,6145294892570,6145294760170', NULL, 'mm', 'real_estate_50l_1c', NULL, 'Chennai', 'act_16819025', 'Social Beat Ad Account', '2019-12-19 22:33:37', '2019-12-19 21:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(65) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `username`, `password`, `email`, `created_time`, `updated_time`) VALUES
(4, 'ravi', '$2y$10$WmXYJ8FXI7kwaXnB92V77.HJlpRtkyZoWMJkKyQTSr9CNdcNbpGY2', NULL, NULL, NULL),
(5, 'ravisb', '$2y$10$XDTaacLyy8h6mNxGlRvJJOUf1wo3s1bIghN1heB.j8rwteOTt3ube', NULL, NULL, NULL),
(6, 'kumar', '$2y$10$555fa/woD3wzvvyTuODyMugL//p0bG21sI9lOT6Tb5jYhdqZD1wVq', NULL, NULL, NULL),
(7, 'sundaram_mf', '$2y$10$ajSYs3d6nMa0U83UZ.LXeeRrOEJMOyw5Oth7du6UynPVND9gnBllO', NULL, NULL, NULL),
(8, 'casagrand', '$2y$10$mBD6J3VavZO9nKVNKrXMH.mfnUbH8kYLuIH4VZGHW0HTZ2NxBIPze', NULL, NULL, NULL),
(9, 'testing_k', '$2y$10$qVmGiVHVHO4720PMSinFae9kAczeidWHHdrx55AmJBmaOfE71UldO', NULL, NULL, NULL),
(10, 'testingxyz', '$2y$10$rApns8BQIIw6wcx/2N.C9OveHZudJVClQ2Xge2pKejeG61YfjMiNS', NULL, NULL, NULL),
(11, 'testingxyz1', '$2y$10$h6b4v84V1rMXVis4T3U82.zIFQIhvPcFJFX8p/3WLtcX3b5I4YuYC', NULL, NULL, NULL),
(12, 'medha', '$2y$10$rGhKwm9FydC.azHVFlaXo.UJDqWDgWJd30gsaWdMgnzhzpRjFsRp2', NULL, NULL, NULL),
(13, 'tester_k', '$2y$10$Lk9JHuqTmhYTXew2TsIbouCSgXWMCo51i5143ejfSAby2rr3IDGXG', NULL, NULL, NULL),
(14, 'testqwwe', '$2y$10$QZAEvdJlnU9RB6oT/GK9kubGsefIdGppVPaM7aoT3M.iQKWW0wQgS', NULL, NULL, NULL),
(15, 'pdsas123', '$2y$10$dv7k03KHKFpTv2l1Bq4PWeBS1UJM4maW.P2xbInHLmhyeAOE1fRtu', NULL, NULL, NULL),
(16, 'sohail', '$2y$10$PxkN.Ilnk/u/WY3wABGBauNx.OU9TX9wKVDXiMOHTyWNwhZbf3ZY2', NULL, NULL, NULL),
(17, 'ytrewq', '$2y$10$xu.S.NFu74kqSNffWHpwzusZCFJaiJgH2UzUzLTHZlwDngaQnexIi', NULL, NULL, NULL),
(18, 'arju', '$2y$10$/2XqalKY7fXvqxbmPMHb.O.hCYQ1LQGfzb5ZqKi9U9UAteLFL.d0K', NULL, NULL, NULL),
(19, 'rengavasan@socialbeat.in', '$2y$10$uLn55.H7P8kNJbZySygRYOjZOxrSL.158h/otldH9Y0u.Bdn6sAWW', NULL, NULL, NULL),
(20, 'kushal', '$2y$10$jU.IVCaN9oc0E8zqxdyqv.C09pXibjIcYQ9cnxhtmvj.nMsz6ExL2', NULL, NULL, NULL),
(21, 'kushaljain', '$2y$10$pEVhUWFkKAIMYyL.W5Qrt.ydHV1FiykldXP0xbsAwVOyi78pjtwnu', NULL, NULL, NULL),
(22, 'xxx', '$2y$10$QOcXHtVf6cIWnUUtK5yyX.aGEkyQH7KYrSB4/wbKPTDOy4kH.syL6', NULL, NULL, NULL),
(23, 'Satvika Rajeev', '$2y$10$XiBtuJs3Yn.Wsfh1tFNd0.AEsgQR15i5k5v5cJegleYeZtQ/V.Ueq', NULL, NULL, NULL),
(24, 'new', '$2y$10$Sj/vaGiLvoOMxBnyno/iHuBRU0e0VRJtStP4O8z0Q2fJJGTYdKUIi', NULL, NULL, NULL),
(25, 'satvika@socialbeat.in', '$2y$10$/3yQyOfHtxzX6ZXcHJWM3OTBQACronjxahWtXHrlzGBg3kM7raCHS', NULL, NULL, NULL),
(26, 'chirag', '$2y$10$L9rCoCWx.IoTyStwsOjdKevbdU4TeZIm6v9PxC/tBbE6HoeQjQWFG', NULL, NULL, NULL),
(27, 'testravi', '$2y$10$HjtGi7XiGd4d7PImdjGaeeCcy/S8ox9ma5lOqVdNvaFG09IwuIwem', NULL, NULL, NULL),
(28, 'kushaltest', '$2y$10$JTjCRN2DwwF3X0Ln4jKNIOyqnxPZn5n/YVjANJEUJu6SDkQ6nK0ge', NULL, NULL, NULL),
(30, 'arjusmoon_sb', '$2y$10$g.VioXUnzsyvNQMt.LYEMue8X/ncyK9J9J8BxKtBY7n52QlGMSGQi', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fb_campaign_details`
--

CREATE TABLE `fb_campaign_details` (
  `id` int(11) NOT NULL,
  `camp_id` varchar(45) DEFAULT NULL,
  `campaign_name` varchar(45) DEFAULT NULL,
  `fb_CPL` double DEFAULT NULL,
  `fb_CPC` double DEFAULT NULL,
  `fb_CTR` double DEFAULT NULL,
  `fb_CPTP` double DEFAULT NULL,
  `fb_CPM` double DEFAULT NULL,
  `fb_CPE` double DEFAULT NULL,
  `camp_duration` varchar(45) DEFAULT NULL,
  `objective` double DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fb_details`
--

CREATE TABLE `fb_details` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `fb_access_token` varchar(255) DEFAULT NULL,
  `fb_account_id` varchar(50) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fb_details`
--

INSERT INTO `fb_details` (`id`, `id_client`, `fb_access_token`, `fb_account_id`, `last_updated`) VALUES
(2, 4, 'EAAFLpgouUzYBAPDMmHi9n14azTXbU0ZArRZCeW9P3TdjC8jgz1f73tuSaHq6hZBhnpQlUBBC36K6gEKCZABdZBYaSUWXemvZAGHcEtO7s0prKT5NfmmV3YsbhxgbtPBmEssqqmQ8bqyb7UtfXYN5z6laOtyuuA4yDcZA2iWxsTz3AZDZD', 'act_1497399147190652', '2019-08-26 11:39:00'),
(3, 5, 'EAAFLpgouUzYBAOdZAByKYFc0tHS15FoHVhEprHLarm3Red7fmirxbjgXzjSBCZCRjbwbGe9N2ZBU1d78qeuZCLQ7KnlBeZA1MM14S6bnKJx6Afmjfo24VYz43VF9oLzuK3VUIf2pjTgWdcj6ZAAknZAbsz7YZB4etXZCflq6U8QM8ngZDZD', 'act_277103256174822', '2019-08-30 11:38:59'),
(4, 6, 'EAAFLpgouUzYBAPFiZBpenp67ioJVqbAqdDgYsil5BRabpiYjgEUO7ZAiQs3vZAmtfrTixnsJnNxTcNbWo6DbZATe7khnfyzGSyCuSxvIgDsllRn4WuBQYo3K4atRaZA1zLmiGOQDrTEm2VfOilJk9TLIpZAxzYNkEZD', 'act_485556621925708', '2019-08-27 11:16:23'),
(5, 7, 'EAAFLpgouUzYBAKUsBfIcQQ06qzZAgUQMGpdgdi2HiHFwilBVqd3K4Qh0ISNAt1WvcGue79SB4jZB6qXh8koeIgpXDNSsZAWfZBeyx3UuR6aJgRQMOYZCGS7j1ZAZAorZBOG0ivKN3Atl0MlYnoH4qydUGxXZBwhZAZAZALfSeY4e03QZBLwZDZD', 'act_110172152506718', '2019-09-04 05:18:56'),
(6, 8, 'EAAFLpgouUzYBACLM11uQBaKS8YDL3INjVuFz0bIMA6KdL1vPYDFcraREAICP13FFzWq4detqk1Ge4V7j4CGldhpJtXgLgL9FZClJDabMH1EHGCLZBhXtNBl81ymYygbRRNKZBGvkV5JRmbUYJrEINEwRWVPJa3UNZA6BbtxXHAZDZD', 'act_329919047566786', '2019-12-03 11:02:04'),
(7, 9, 'EAAFLpgouUzYBAHIJlSX6BANZB1miQk4FIhPlMKwuYuKmyooI0ZAUoHkV0scw2Fdz6Tcs89dr5DKR8c39s1XX7As6ybGSA6lFWUyhdmT9YoN9Qhd7hE3E5irYOTyB6vAAXCAZCzlySSYpr3ziZCC1ZAm6XKQeC9ZBDyuvvY49GxaAZDZD', NULL, '2019-09-05 15:41:11'),
(8, 10, 'EAAFLpgouUzYBABL3FGoE1kVPt3qrZA6hwZAnznZBNTnWfcWPXVGTcGUIdV0wmqqC8MVVbNGsjM0wUmxDnKmRi2gCfIpy41Tm1pgZBHHKicV3wB4pmqzep8K8FNNu41qxNOQ9BtdbwoWdCIqZA4YdGorZAQtfls7FYvPq8atTLc5wZDZD', NULL, '2019-09-06 09:49:43'),
(9, 11, 'EAAFLpgouUzYBAEGqV3iKZAaUsCIHSBI3cPrJ6pIUDuiUdX3ZAR1nFlZAvcCfTH09qb39AdblMrumx628tqw5AU1ZAqVtVRotNJZChUahGgfKCcUrjAj8BK4q05ZB7FIjblokShrz9PN1XRIcNncoZBrvGh1TgZCDUOuWP8ZALv7M48gZDZD', NULL, '2019-09-06 11:04:29'),
(10, 12, 'EAAFLpgouUzYBAKPRJHde55GCMN0Ux0NYu6ZB8p4yDEROJ4Sd1McCSAC9I4BgvMc2ta7LBkCdhWSN1QLZBxRqnhHjgqjKir4hzNj65Cb78mdXVKWZAooCy7bWEpVJq8zdgG9M6DZC38JDEY6Ch9ZCao4lIohyWl0Jw5ZBdsVq3BEwZDZD', 'act_123476804416739', '2019-09-06 11:44:44'),
(11, 14, 'EAAFLpgouUzYBACRv28N70RQqpxvwNIM05e5h5TF0mry4ega8AvihLyGNDf54pC0YiFBOpodZB8sxqZAzWPMOokDGFt1YsNwOFBf2mseQtDuCDYvbAualVox2731uniRsBFBlMKTcVwyMqX2246sZAoeFMaiOKZC3vXi3Sx5swAZDZD', NULL, '2019-09-08 14:54:30'),
(12, 15, 'EAAFLpgouUzYBADdqOpSyxOEme8ap7vVec5RlUYICDtMmhcNVyoncZCJLP7C3QwsQu3PDMEqPy6DghbPTLHT0rmtxsiC4Irig62DDWZBfekCJgZCCsuzreysmAFG6BwUA8BiK7eMqcc1iXYeIiYpcBxEdkRrARpj1ewYh46MFQZDZD', NULL, '2019-09-09 06:50:47'),
(13, 16, 'EAAFLpgouUzYBAE6rnZAS15R7wNtqQSWheeelKh6HG1NiTfO5PaOAliuZCfGYrHsZBMnXXDdQlpEuK9pnnfeH4QbUBk0fEZBDNImnFam3lAakmjdYw7LoqqZBYWjK1jwVr9b1ZCz9dH9ni5ZC1v2QUJ7qn9HHNTLo44ZD', NULL, '2019-09-09 06:57:00'),
(14, 18, 'EAAFLpgouUzYBAByqP6oGi7gYhihoUawurXOtM40dsf4ermbWmpVipWaRI1JbrBK0GHOdWLHmOcXyy4TSWZASP2lioHctGRfq1ZBTvS7Y4sIiyWvvGS324ZBE8NpcJnv5IWk0WyItPZCW2Mw0YT4nZABW2HcSAnaEPAfcELU3rQAZDZD', NULL, '2019-09-30 11:37:08'),
(15, 19, 'EAAFLpgouUzYBAHsYlgMlXC8fxzW2auJRWB0U5B4ZBnAMJpgmyoZCOJprm7TyPkOjyekKhqKN7EEvyAZAljaMWCvmNUdPF8XWKjDHr2oKSLHUFai7jAgs7qZAfIZB4UZCMZAerGSBEN4CZCgZAdopniY86QQjhZA1Ose58uLx9EQkJfuAZDZD', NULL, '2019-11-04 07:39:07'),
(16, 20, 'EAAFLpgouUzYBABZBrSGpZB66ZBru7M7lVAdGRtnqr775ZAZAa4X7dZBhIVjL4LDhA1lj2nTRfdRBejelZA59P3v3tpRUra2cYZAmeyV5jT8EwnVjZBZAoF05uAl6ti4QuvZBdKATC39R5ZCnFn9j8J35Q4DKJNTUnkCW0vXXJWiWRKWBIgZDZD', NULL, '2019-12-03 10:16:37'),
(17, 21, 'EAAFLpgouUzYBADAoHLOX5nmKDThg4ClGz1lNk24FO8sxSEagXr3ZAdaP6S7XZCgfNxDoZBQ9ZBgzNfleJs87Wwk9ntLHhgHcMZAjdlZCnP2pWcp9NhSVICyXtG5ZBLxtxb9LXd8CR5oFUZAjgcpZB7cZBjmkgXqBGha916Huw5CwqbFAZDZD', NULL, '2019-11-04 13:21:35'),
(18, 22, 'EAAFLpgouUzYBABcJ1HVpqRdfX2Ps6c6yMg1RYrpWk0SutEaxIT4TAZBWCPZB9qLbB2FqcknTPHkhLLoHhQxrmjcmtO2xnuvcrKD2V3NbZCOTaSH5ns7ZCqyMZAPhy36dBsRCbVGQOb7erMZBxMqoFe9FOHulMvlnSwL9AVtjEvLwZDZD', NULL, '2019-11-15 13:07:44'),
(20, 24, 'EAAFLpgouUzYBAGRmSIFUmE0vkEDcF7TnXxaoexYlUhsKs8Cl6aKYTD40YmJROfgBuoHrkaOFZC91kJxsbifsYhByqR2mwgnLw79fMcI4q7aA8BSBcn4AwvhO2tH3U8QcsZBVRwnBuYRd07HBkM3k1Mx6eJnScO0Odh17H9mwZDZD', NULL, '2019-11-19 13:01:37'),
(21, 25, 'EAAFLpgouUzYBAJQjfNr8SLGjLqWPsfwYk2CwePFUZAlh3wTlCy4NZCsmfP8O4hTGoMD6nShDPGi6s9bFblklrqvofXXqzxgBRA9iqQzxKjDSZBWE0lL571XrtGNRljZBH4d2mlb8AhxdqWaDd5fKXVmM4LPy9AJrQBLvkNOmkK6l6KFiJa7m', NULL, '2019-12-02 08:09:55'),
(22, 23, 'EAAFLpgouUzYBAGNVTHM05tLttHF2Plz4tfXbdfZBMPilHY60tabNDpezPNdw2gfgBc8SVyBKDtnA4ZBZB9CFZC7C7LzW8pUFxbonD0HJvMazeZAd32rKdTEZA2CLlGEFVZBmBqMPEtto9qix2VpQoJxuhRM5LQuTWmdKdOx47bVTjpZA0vBs5w9L', NULL, '2019-12-02 08:11:48'),
(23, 26, 'EAAFLpgouUzYBAGQNj1fYPAUZAjscafQjYLZCanUqv4d4uIHVkW5KsgtDZB3qUPSHKJ2KOnDw26F0vBRXWEskusZAj4cjoegwK2qvZA6El6bcVhziEzvtkKPHsUpnPm26zvwaMaYFngqWoTEZCDxiY2s43bS3ZBzDAYnbt7XahYcXwZDZD', NULL, '2019-12-02 07:46:50'),
(24, 27, 'EAAFLpgouUzYBAGxdkrAUdg8t25dQrvggt5FQrbzVxuN0T0WXFg9vt1vojICio2dGBSSJeWsF8TaGxjiwVW6rEDY3UTdTQB6IYJ6N1EQKJKEIDrZAa35ZAOsIZBPtalTizfDFWoEtzG31ySP7G8h1prhfNqWeUhKkhFk4aF2jAZDZD', NULL, '2019-12-02 09:33:22'),
(25, 28, 'EAAFLpgouUzYBAAatZAKHZA6xH3y8JZB1kDNgVG3xpMfiChQWsjIVU9hFpewovzPYbyMkRedOr6xHQOrQ1jDI0KskmNZANTFOYj3aGH9EHPZACQxBFpaKQpuxqIA6V2AndvpsZBUY4iD944zIZBzaG07LGvEWhVhuCZBXZBBXRYvQqzQZDZD', NULL, '2019-12-03 10:17:20');

-- --------------------------------------------------------

--
-- Table structure for table `insights_codenames`
--

CREATE TABLE `insights_codenames` (
  `id` int(11) NOT NULL,
  `codename` varchar(255) NOT NULL,
  `actual_name` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insights_codenames`
--

INSERT INTO `insights_codenames` (`id`, `codename`, `actual_name`, `category`) VALUES
(1, 'A', 'cpc', 'A'),
(2, 'AA', 'b_cpc', 'A'),
(3, 'B', 'cpm', 'B'),
(4, 'BB', 'b_cpm', 'B'),
(5, 'C', 'cpl', 'C'),
(6, 'CC', 'b_cpl', 'C'),
(7, 'D', 'ctr', 'D'),
(8, 'DD', 'b_ctr', 'D'),
(9, 'E', 'cptp', 'E'),
(10, 'EE', 'b_cptp', 'E'),
(11, 'F', 'cpe', 'F'),
(12, 'FF', 'b_cpe', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `insights_formulas`
--

CREATE TABLE `insights_formulas` (
  `id` int(11) NOT NULL,
  `formula` varchar(255) NOT NULL,
  `insight` text NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insights_formulas`
--

INSERT INTO `insights_formulas` (`id`, `formula`, `insight`, `category`) VALUES
(1, '$A>$AA', 'Oh no! Your campaign seems to be a little expensive!<br>Refresh your creative to increase relevance score.', 'A'),
(2, '$A<$AA', 'This is great! Your campaign\'s CPCs are lower than the industry benchmarks!<br>Check for the quality of these leads and your campaign is right on track!', 'A'),
(3, '$B>$BB', 'Oh no! Your campaign seems to be a little expensive!<br>Refresh your creative to increase relevance score.', 'B'),
(4, '$B<$BB', 'This is great! Your campaign\'s CPM is lower than the industry benchmarks!<br>Check your frequency & unique reach to ensure your campaign is right on track!', 'B'),
(5, '$C>$CC', 'Oh no! Your CPL seems to be higher than industry benchmarks!\r\n<br>Here are a few steps to bring your campaign back on track!\r\n<br><br>Step 1: Relook at your targeting strategy and reduce depenendency on core audiences. Explore Custom & Lookalike audiences\r\n<br>Step 2: Check for Audience Overlap and avoid any overlap over 10-15%\r\n<br>Step 3: Relook your bidding strategy\r\n<br>Step 4: Make sure your ad creatives are not text heavy\r\n<br>Step 5: Refresh your creative to increase relevance score', 'C'),
(6, '$C<$CC', 'This is great! Your campaign\'s CPLs are lower than the industry benchmarks!<br>Check for the quality of these leads and your campaign is right on track!', 'C'),
(7, '$D>$DD', 'This is great! Your campaign\'s CTR is higher than the industry benchmarks!<br>Check for the quality of these leads and your campaign is right on track!', 'D'),
(8, '$D<$DD', 'Oh no! Your campaigns seem to be expensive<br>Refresh your creative to increase relevance scores & CTR.', 'D'),
(9, '$E>$EE', 'Oh no! Your campaign seems to be a little expensive!<br>Refresh your creative to increase relevance score.', 'E'),
(10, '$E<$EE', 'This is great! Your campaigns are lower than the industry benchmarks!<br>Check for the quality of these leads and your campaign is right on track!', 'E'),
(11, '$F>$FF', 'Oh no! Your campaign seems to be a little expensive!<br>Refresh your creative to increase relevance score.', 'F'),
(12, '$F<$FF', 'This is great! Your campaigns CPE is lower than the industry benchmarks!<br>Check your frequency & unique reach to ensure your campaign is right on track!', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_commentmeta`
--

CREATE TABLE `wpqd_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_comments`
--

CREATE TABLE `wpqd_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wpqd_comments`
--

INSERT INTO `wpqd_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-05-02 15:35:27', '2019-05-02 15:35:27', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_gf_draft_submissions`
--

CREATE TABLE `wpqd_gf_draft_submissions` (
  `uuid` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `submission` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_gf_entry`
--

CREATE TABLE `wpqd_gf_entry` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT 0,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_agent` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `currency` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_status` varchar(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `transaction_id` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'active'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wpqd_gf_entry`
--

INSERT INTO `wpqd_gf_entry` (`id`, `form_id`, `post_id`, `date_created`, `date_updated`, `is_starred`, `is_read`, `ip`, `source_url`, `user_agent`, `currency`, `payment_status`, `payment_date`, `payment_amount`, `payment_method`, `transaction_id`, `is_fulfilled`, `created_by`, `transaction_type`, `status`) VALUES
(1, 1, NULL, '2019-05-03 05:24:45', NULL, 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(2, 1, NULL, '2019-05-03 05:31:27', NULL, 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(3, 1, NULL, '2019-05-03 06:11:10', NULL, 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(4, 1, NULL, '2019-05-03 07:42:23', NULL, 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(5, 1, NULL, '2019-05-03 09:03:30', NULL, 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(6, 1, NULL, '2019-05-03 09:16:57', NULL, 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(7, 1, NULL, '2019-05-03 10:18:24', NULL, 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(8, 1, NULL, '2019-05-03 11:11:34', '2019-05-03 11:11:35', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(9, 1, NULL, '2019-05-03 11:21:50', '2019-05-03 11:21:51', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(10, 1, NULL, '2019-05-03 11:39:13', '2019-05-03 11:39:14', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(11, 1, NULL, '2019-05-03 11:50:13', '2019-05-03 11:50:13', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(12, 1, NULL, '2019-05-03 12:13:48', '2019-05-03 12:13:48', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(13, 1, NULL, '2019-05-03 17:54:43', '2019-05-03 17:54:43', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(14, 1, NULL, '2019-05-03 18:11:01', '2019-05-03 18:11:01', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(15, 1, NULL, '2019-05-03 23:23:15', '2019-05-03 23:23:16', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(16, 1, NULL, '2019-05-04 07:17:26', '2019-05-04 07:17:26', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(17, 1, NULL, '2019-05-04 07:18:50', '2019-05-04 07:18:51', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(18, 1, NULL, '2019-05-04 07:19:16', '2019-05-04 07:19:16', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(19, 1, NULL, '2019-05-05 08:27:24', '2019-05-05 08:27:25', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(20, 1, NULL, '2019-05-05 18:48:25', '2019-05-05 18:48:26', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(21, 1, NULL, '2019-05-05 19:08:15', '2019-05-05 19:08:16', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(22, 1, NULL, '2019-05-05 19:49:26', '2019-05-05 19:49:26', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(23, 1, NULL, '2019-05-05 23:17:30', '2019-05-05 23:17:30', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(24, 1, NULL, '2019-05-06 02:26:07', '2019-05-06 02:26:07', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(25, 1, NULL, '2019-05-06 03:11:42', '2019-05-06 03:11:42', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(26, 1, NULL, '2019-05-06 08:04:14', '2019-05-06 08:04:15', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(27, 1, NULL, '2019-05-06 08:42:03', '2019-05-06 08:42:03', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(28, 1, NULL, '2019-05-06 08:45:42', '2019-05-06 08:45:42', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(29, 1, NULL, '2019-05-06 09:59:27', '2019-05-06 09:59:27', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(30, 1, NULL, '2019-05-06 10:02:33', '2019-05-06 10:02:35', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(31, 1, NULL, '2019-05-06 11:05:35', '2019-05-06 11:05:35', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(32, 1, NULL, '2019-05-06 11:32:17', '2019-05-06 11:32:18', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(33, 1, NULL, '2019-05-06 11:40:19', '2019-05-06 11:40:19', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(34, 1, NULL, '2019-05-06 11:42:16', '2019-05-06 11:42:16', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(35, 1, NULL, '2019-05-06 12:17:20', '2019-05-06 12:17:21', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(36, 1, NULL, '2019-05-06 13:38:48', '2019-05-06 13:38:48', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(37, 1, NULL, '2019-05-07 04:38:23', '2019-05-07 04:38:23', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(38, 1, NULL, '2019-05-07 04:39:36', '2019-05-07 04:39:36', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(39, 1, NULL, '2019-05-07 07:03:06', '2019-05-07 07:03:06', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(40, 1, NULL, '2019-05-07 07:40:34', '2019-05-07 07:40:34', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(41, 1, NULL, '2019-05-08 06:21:50', '2019-05-08 06:21:50', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(42, 1, NULL, '2019-05-08 09:08:54', '2019-05-08 09:08:54', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(43, 1, NULL, '2019-05-10 10:02:19', '2019-05-10 10:02:19', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(44, 1, NULL, '2019-05-11 09:41:11', '2019-05-11 09:41:11', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(45, 1, NULL, '2019-05-11 10:27:07', '2019-05-11 10:27:07', 0, 1, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(46, 1, NULL, '2019-05-11 11:46:52', '2019-05-11 11:46:52', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(47, 1, NULL, '2019-05-11 11:51:51', '2019-05-11 11:51:51', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(48, 1, NULL, '2019-05-11 12:04:11', '2019-05-11 12:04:12', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(49, 1, NULL, '2019-05-11 12:08:09', '2019-05-11 12:08:10', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(50, 1, NULL, '2019-05-13 14:13:44', '2019-05-13 14:13:45', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(51, 1, NULL, '2019-05-13 14:19:09', '2019-05-13 14:19:09', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(52, 1, NULL, '2019-05-13 14:20:21', '2019-05-13 14:20:22', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(53, 1, NULL, '2019-05-13 14:21:29', '2019-05-13 14:21:29', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(54, 1, NULL, '2019-05-13 14:22:19', '2019-05-13 14:22:19', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(55, 1, NULL, '2019-05-13 14:23:09', '2019-05-13 14:23:11', 0, 1, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(56, 1, NULL, '2019-05-13 14:32:36', '2019-05-13 14:32:36', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(57, 1, NULL, '2019-05-15 12:35:07', '2019-05-15 12:35:07', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_gf_entry_meta`
--

CREATE TABLE `wpqd_gf_entry_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `entry_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `item_index` varchar(60) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wpqd_gf_entry_meta`
--

INSERT INTO `wpqd_gf_entry_meta` (`id`, `form_id`, `entry_id`, `meta_key`, `meta_value`, `item_index`) VALUES
(1, 1, 1, '1', 'test SB', NULL),
(2, 1, 1, '2', 'xv@dfg.df', NULL),
(3, 1, 1, '3', '5676767575', NULL),
(4, 1, 1, '4', 'http://www.adsquotient.com/', NULL),
(5, 1, 2, '1', 'test', NULL),
(6, 1, 2, '2', 'test@gmail.com', NULL),
(7, 1, 2, '3', '1234567890', NULL),
(8, 1, 2, '4', 'http://www.adsquotient.com/', NULL),
(9, 1, 3, '1', 'test SB', NULL),
(10, 1, 3, '2', 'test@gmail.com', NULL),
(11, 1, 3, '3', '9898888886', NULL),
(12, 1, 3, '4', 'http://www.adsquotient.com/', NULL),
(13, 1, 4, '1', 'test', NULL),
(14, 1, 4, '2', 'test@gmail.com', NULL),
(15, 1, 4, '3', '1212121212', NULL),
(16, 1, 4, '4', 'http://www.adsquotient.com/', NULL),
(17, 1, 5, '1', 'Ejdj', NULL),
(18, 1, 5, '2', 'djjd@hsjs.dnd', NULL),
(19, 1, 5, '3', '9898988787', NULL),
(20, 1, 5, '4', 'http://www.adsquotient.com/', NULL),
(21, 1, 6, '1', 'test SB', NULL),
(22, 1, 6, '2', 'test@gmail.com', NULL),
(23, 1, 6, '3', '9898989898', NULL),
(24, 1, 6, '4', 'http://www.adsquotient.com/', NULL),
(25, 1, 7, '1', 'test SB', NULL),
(26, 1, 7, '2', 'test@gmail.com', NULL),
(27, 1, 7, '3', '9898989890', NULL),
(28, 1, 7, '4', 'http://www.adsquotient.com/', NULL),
(29, 1, 8, '1', 'test', ''),
(30, 1, 8, '2', 'ttrt@dfgdf.sdf', ''),
(31, 1, 8, '3', '6767676744', ''),
(32, 1, 8, '4', 'http://www.adsquotient.com/', ''),
(33, 1, 9, '1', 'Terdd', ''),
(34, 1, 9, '2', 'hdhd@jsjd.dnj', ''),
(35, 1, 9, '3', '9873733373', ''),
(36, 1, 9, '4', 'http://www.adsquotient.com/', ''),
(37, 1, 10, '1', 'Teyr', ''),
(38, 1, 10, '2', 'shhd@dhd.dh', ''),
(39, 1, 10, '3', '8473837373', ''),
(40, 1, 10, '4', 'http://www.adsquotient.com/', ''),
(41, 1, 11, '1', 'Jdjd', ''),
(42, 1, 11, '2', 'bdjd@dhsj.dmkd', ''),
(43, 1, 11, '3', '7333337483', ''),
(44, 1, 11, '4', 'http://www.adsquotient.com/', ''),
(45, 1, 12, '1', 'panisa', ''),
(46, 1, 12, '2', 'panisa@socialbeat.in', ''),
(47, 1, 12, '3', '9176711072', ''),
(48, 1, 12, '4', 'http://www.adsquotient.com/', ''),
(49, 1, 13, '1', 'testshaha', ''),
(50, 1, 13, '2', 'tetst@gmail.com', ''),
(51, 1, 13, '3', '9878987654', ''),
(52, 1, 13, '4', 'http://www.adsquotient.com/', ''),
(53, 1, 14, '1', 'TestShalini', ''),
(54, 1, 14, '2', 'test@gmail.com', ''),
(55, 1, 14, '3', '9988776655', ''),
(56, 1, 14, '4', 'http://www.adsquotient.com/', ''),
(57, 1, 15, '1', 'Test vel', ''),
(58, 1, 15, '2', 'testvel@gmail.com', ''),
(59, 1, 15, '3', '8765123467', ''),
(60, 1, 15, '4', 'http://www.adsquotient.com/', ''),
(61, 1, 16, '1', 'test', ''),
(62, 1, 16, '2', 'test@gmail.com', ''),
(63, 1, 16, '3', '1234567890', ''),
(64, 1, 16, '4', 'http://www.adsquotient.com/', ''),
(65, 1, 17, '1', 'Sowmiya', ''),
(66, 1, 17, '2', 'sowmiya@sb.in', ''),
(67, 1, 17, '3', '8787870808', ''),
(68, 1, 17, '4', 'http://www.adsquotient.com/', ''),
(69, 1, 18, '1', 'Shalini ', ''),
(70, 1, 18, '2', 'test@gmail.com', ''),
(71, 1, 18, '3', '9876054321', ''),
(72, 1, 18, '4', 'http://www.adsquotient.com/', ''),
(73, 1, 19, '1', 'Karthikeyan', ''),
(74, 1, 19, '2', 'prodigypk86@gmail.com', ''),
(75, 1, 19, '3', '7200073075', ''),
(76, 1, 19, '4', 'http://www.adsquotient.com/', ''),
(77, 1, 20, '1', 'suneil', ''),
(78, 1, 20, '2', 'suneil@socialbeat.in', ''),
(79, 1, 20, '3', '9990900000', ''),
(80, 1, 20, '4', 'http://www.adsquotient.com/', ''),
(81, 1, 21, '1', '5ccf3507965c7', ''),
(82, 1, 21, '2', 'brad.frick@gmail.com', ''),
(83, 1, 21, '4', 'http://www.adsquotient.com/', ''),
(84, 1, 22, '1', '5ccf3e451ebc7', ''),
(85, 1, 22, '2', 'sjnphx@yahoo.com', ''),
(86, 1, 22, '4', 'http://www.adsquotient.com/', ''),
(87, 1, 23, '1', '5ccf6f7304269', ''),
(88, 1, 23, '2', 'raffaelem1953@gmail.com', ''),
(89, 1, 23, '4', 'http://www.adsquotient.com/', ''),
(90, 1, 24, '1', '5ccf9b3c365cf', ''),
(91, 1, 24, '2', 'misallati@gmail.com', ''),
(92, 1, 24, '4', 'http://www.adsquotient.com/', ''),
(93, 1, 25, '1', '5ccfa5f2b352f', ''),
(94, 1, 25, '2', 'caitlyncorace@gmail.com', ''),
(95, 1, 25, '4', 'http://www.adsquotient.com/', ''),
(96, 1, 26, '1', 'test SB', ''),
(97, 1, 26, '2', 'test@gmail.com', ''),
(98, 1, 26, '3', '9898989800', ''),
(99, 1, 26, '4', 'http://www.adsquotient.com/', ''),
(100, 1, 27, '1', '5ccff35b36c9f', ''),
(101, 1, 27, '2', 'lapagie@gmail.com', ''),
(102, 1, 27, '4', 'http://www.adsquotient.com/', ''),
(103, 1, 28, '1', '5ccff4363ed12', ''),
(104, 1, 28, '2', 'snowtop@gmail.com', ''),
(105, 1, 28, '4', 'http://www.adsquotient.com/', ''),
(106, 1, 29, '1', '5cd0057ecf773', ''),
(107, 1, 29, '2', 'fatima386@aol.com', ''),
(108, 1, 29, '4', 'http://www.adsquotient.com/', ''),
(109, 1, 30, '1', 'testsha', ''),
(110, 1, 30, '2', 'test@gmail.com', ''),
(111, 1, 30, '3', '1234567890', ''),
(112, 1, 30, '4', 'http://www.adsquotient.com/', ''),
(113, 1, 31, '1', '5cd014fe87ecf', ''),
(114, 1, 31, '2', 'adavis3040@gmail.com', ''),
(115, 1, 31, '4', 'http://www.adsquotient.com/', ''),
(116, 1, 32, '1', '5cd01b41aab2d', ''),
(117, 1, 32, '2', 'stumo99@gmail.com', ''),
(118, 1, 32, '4', 'http://www.adsquotient.com/', ''),
(119, 1, 33, '1', 'shalini', ''),
(120, 1, 33, '2', 'test@gmail.com', ''),
(121, 1, 33, '3', '1234567890', ''),
(122, 1, 33, '4', 'http://www.adsquotient.com/', ''),
(123, 1, 34, '1', 'testsha', ''),
(124, 1, 34, '2', 'test@gmail.com', ''),
(125, 1, 34, '3', '9878987654', ''),
(126, 1, 34, '4', 'http://www.adsquotient.com/', ''),
(127, 1, 35, '1', '5cd025d0a757a', ''),
(128, 1, 35, '2', 'jordyboymez@aol.com', ''),
(129, 1, 35, '4', 'http://www.adsquotient.com/', ''),
(130, 1, 36, '1', '5cd038e728378', ''),
(131, 1, 36, '2', 'ryan.chosnyk@gmail.com', ''),
(132, 1, 36, '4', 'http://www.adsquotient.com/', ''),
(133, 1, 37, '1', 'TestShalini', ''),
(134, 1, 37, '2', 'test@gmail.com', ''),
(135, 1, 37, '3', '9988776655', ''),
(136, 1, 37, '4', 'http://www.adsquotient.com/', ''),
(137, 1, 38, '1', 'TestShalini', ''),
(138, 1, 38, '2', 'test@gmail.com', ''),
(139, 1, 38, '3', '9988776655', ''),
(140, 1, 38, '4', 'http://www.adsquotient.com/', ''),
(141, 1, 39, '1', 'Shalini', ''),
(142, 1, 39, '2', 'suneil@socialbeat.in', ''),
(143, 1, 39, '3', '0000999999', ''),
(144, 1, 39, '4', 'http://www.adsquotient.com/', ''),
(145, 1, 40, '1', 'test SB', ''),
(146, 1, 40, '2', 'test@gmail.com', ''),
(147, 1, 40, '3', '9696969696', ''),
(148, 1, 40, '4', 'http://www.adsquotient.com/', ''),
(149, 1, 41, '1', 'Priya ', ''),
(150, 1, 41, '2', 'jainpriya7086@gmail.com', ''),
(151, 1, 41, '3', '8109726845', ''),
(152, 1, 41, '4', 'http://www.adsquotient.com/', ''),
(153, 1, 42, '1', 'Jugmendra Baliyan', ''),
(154, 1, 42, '2', 'jugmendra.india@gmail.com', ''),
(155, 1, 42, '3', '8802166111', ''),
(156, 1, 42, '4', 'http://www.adsquotient.com/', ''),
(157, 1, 43, '1', 'Julien Collins', ''),
(158, 1, 43, '2', 'jaylon.heaney98@ourtimesupport.com', ''),
(159, 1, 43, '3', '2633717358', ''),
(160, 1, 43, '4', 'http://www.adsquotient.com/', ''),
(161, 1, 44, '1', 'test', ''),
(162, 1, 44, '2', 'test@gmail.com', ''),
(163, 1, 44, '3', '1234567890', ''),
(164, 1, 44, '4', 'http://www.adsquotient.com/', ''),
(165, 1, 45, '1', 'testsha', ''),
(166, 1, 45, '2', 'test@gmail.com', ''),
(167, 1, 45, '3', '1234567890', ''),
(168, 1, 45, '4', 'http://www.adsquotient.com/', ''),
(169, 1, 46, '1', 'shalini', ''),
(170, 1, 46, '2', 'test@gmail.com', ''),
(171, 1, 46, '3', '1234567890', ''),
(172, 1, 46, '4', 'http://www.adsquotient.com/', ''),
(173, 1, 47, '1', 'kumar', ''),
(174, 1, 47, '2', 'kumar@gmail.com', ''),
(175, 1, 47, '3', '8529637415', ''),
(176, 1, 47, '4', 'http://www.adsquotient.com/', ''),
(177, 1, 48, '1', 'kumar', ''),
(178, 1, 48, '2', 'kumar@gmail.com', ''),
(179, 1, 48, '3', '8529631471', ''),
(180, 1, 48, '4', 'http://www.adsquotient.com/', ''),
(181, 1, 49, '1', 'kumar', ''),
(182, 1, 49, '2', 'kumar@gmail.com', ''),
(183, 1, 49, '3', '1234567895', ''),
(184, 1, 49, '4', 'http://www.adsquotient.com/', ''),
(185, 1, 50, '1', 'test', ''),
(186, 1, 50, '2', 'test@gmail.com', ''),
(187, 1, 50, '3', '8954874578', ''),
(188, 1, 50, '4', 'http://www.adsquotient.com/', ''),
(189, 1, 51, '1', 'test', ''),
(190, 1, 51, '2', 'test@gmail.com', ''),
(191, 1, 51, '3', '8954874578', ''),
(192, 1, 51, '4', 'http://www.adsquotient.com/', ''),
(193, 1, 52, '1', 'tsttt', ''),
(194, 1, 52, '2', 'tsttt@gmail.com', ''),
(195, 1, 52, '3', '8954874578', ''),
(196, 1, 52, '4', 'http://www.adsquotient.com/', ''),
(197, 1, 53, '1', 'ccc', ''),
(198, 1, 53, '2', 'ccc@gmial.com', ''),
(199, 1, 53, '3', '8578457845', ''),
(200, 1, 53, '4', 'http://www.adsquotient.com/', ''),
(201, 1, 54, '1', 'ranan', ''),
(202, 1, 54, '2', 'ranan@gmail.com', ''),
(203, 1, 54, '3', '9865478452', ''),
(204, 1, 54, '4', 'http://www.adsquotient.com/', ''),
(205, 1, 55, '1', 'rrrrae', ''),
(206, 1, 55, '2', 'rrrrae@gmail.com', ''),
(207, 1, 55, '3', '8945784578', ''),
(208, 1, 55, '4', 'http://www.adsquotient.com/', ''),
(209, 1, 56, '1', 'etsf', ''),
(210, 1, 56, '2', 'etsf@gmail.com', ''),
(211, 1, 56, '3', '8954874578', ''),
(212, 1, 56, '4', 'http://www.adsquotient.com/', ''),
(213, 1, 57, '1', 'hitesh mali', ''),
(214, 1, 57, '2', 'malih6854@gmail.com', ''),
(215, 1, 57, '3', '7738791498', ''),
(216, 1, 57, '4', 'http://www.adsquotient.com/', '');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_gf_entry_notes`
--

CREATE TABLE `wpqd_gf_entry_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `entry_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `note_type` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `sub_type` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_gf_form`
--

CREATE TABLE `wpqd_gf_form` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_trash` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wpqd_gf_form`
--

INSERT INTO `wpqd_gf_form` (`id`, `title`, `date_created`, `date_updated`, `is_active`, `is_trash`) VALUES
(1, 'Enquire now', '2019-05-02 15:59:56', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_gf_form_meta`
--

CREATE TABLE `wpqd_gf_form_meta` (
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `entries_grid_meta` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `confirmations` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `notifications` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wpqd_gf_form_meta`
--

INSERT INTO `wpqd_gf_form_meta` (`form_id`, `display_meta`, `entries_grid_meta`, `confirmations`, `notifications`) VALUES
(1, '{\"title\":\"Enquire now\",\"description\":\"\",\"labelPlacement\":\"top_label\",\"descriptionPlacement\":\"below\",\"button\":{\"type\":\"text\",\"text\":\"Submit\",\"imageUrl\":\"\"},\"fields\":[{\"type\":\"text\",\"id\":1,\"label\":\"\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"Name\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1},{\"type\":\"text\",\"id\":2,\"label\":\"\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"Email\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1},{\"type\":\"text\",\"id\":3,\"label\":\"\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"Phone\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1},{\"type\":\"hidden\",\"id\":4,\"label\":\"URL\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1}],\"version\":\"2.2.5.5\",\"id\":1,\"useCurrentUserAsAuthor\":true,\"postContentTemplateEnabled\":false,\"postTitleTemplateEnabled\":false,\"postTitleTemplate\":\"\",\"postContentTemplate\":\"\",\"lastPageButton\":null,\"pagination\":null,\"firstPageCssClass\":null}', NULL, '{\"5ccb13fc168bd\":{\"id\":\"5ccb13fc168bd\",\"name\":\"Default Confirmation\",\"isDefault\":true,\"type\":\"message\",\"message\":\"Thanks for contacting us! We will get in touch with you shortly.\",\"url\":\"\",\"pageId\":\"\",\"queryString\":\"\"}}', '{\"5ccb13fc0fb8f\":{\"isActive\":true,\"name\":\"Admin Notification\",\"service\":\"wordpress\",\"event\":\"form_submission\",\"to\":\"{admin_email}\",\"toType\":\"email\",\"cc\":\"\",\"bcc\":\"\",\"subject\":\"New submission from {form_title}\",\"message\":\"{all_fields}\",\"from\":\"admin@adsquotient.com\",\"fromName\":\"\",\"replyTo\":\"\",\"routing\":null,\"conditionalLogic\":null,\"disableAutoformat\":false,\"enableAttachments\":false,\"id\":\"5ccb13fc0fb8f\"}}');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_gf_form_revisions`
--

CREATE TABLE `wpqd_gf_form_revisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_gf_form_view`
--

CREATE TABLE `wpqd_gf_form_view` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `count` mediumint(8) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_gf_rest_api_keys`
--

CREATE TABLE `wpqd_gf_rest_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_links`
--

CREATE TABLE `wpqd_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_options`
--

CREATE TABLE `wpqd_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wpqd_options`
--

INSERT INTO `wpqd_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://www.adsquotient.com', 'yes'),
(2, 'home', 'http://www.adsquotient.com', 'yes'),
(3, 'blogname', 'adsquotient', 'yes'),
(4, 'blogdescription', 'My WordPress Blog', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'admin@beat.social', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:91:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:20:\"gravityformsapi/(.*)\";s:33:\"index.php?gfapi_route=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=6&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:29:\"gravityforms/gravityforms.php\";i:1;s:29:\"easy-wp-smtp/easy-wp-smtp.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'quotient', 'yes'),
(41, 'stylesheet', 'quotient', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:29:\"easy-wp-smtp/easy-wp-smtp.php\";s:17:\"swpsmtp_uninstall\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '6', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wpqd_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:6:{i:1557930927;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1557934527;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1557934587;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1557934590;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1557935887;a:1:{s:17:\"gravityforms_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(114, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1556812386;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(284, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.2-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.2-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1557891410;s:15:\"version_checked\";s:5:\"5.1.1\";s:12:\"translations\";a:0:{}}', 'no'),
(782, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1557891418;s:7:\"checked\";a:3:{s:29:\"easy-wp-smtp/easy-wp-smtp.php\";s:7:\"1.3.9.1\";s:29:\"gravityforms/gravityforms.php\";s:5:\"2.4.8\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:5:\"3.1.5\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:29:\"easy-wp-smtp/easy-wp-smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/easy-wp-smtp\";s:4:\"slug\";s:12:\"easy-wp-smtp\";s:6:\"plugin\";s:29:\"easy-wp-smtp/easy-wp-smtp.php\";s:11:\"new_version\";s:7:\"1.3.9.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/easy-wp-smtp/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/plugin/easy-wp-smtp.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/easy-wp-smtp/assets/icon-128x128.png?rev=1242044\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/easy-wp-smtp/assets/banner-772x250.png?rev=1650323\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/really-simple-ssl\";s:4:\"slug\";s:17:\"really-simple-ssl\";s:6:\"plugin\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:11:\"new_version\";s:5:\"3.1.5\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/really-simple-ssl/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/really-simple-ssl.3.1.5.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:70:\"https://ps.w.org/really-simple-ssl/assets/icon-128x128.png?rev=1782452\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/really-simple-ssl/assets/banner-772x250.jpg?rev=1881345\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(780, '_site_transient_timeout_theme_roots', '1557893216', 'no'),
(781, '_site_transient_theme_roots', 'a:1:{s:8:\"quotient\";s:7:\"/themes\";}', 'no'),
(288, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1557891417;s:7:\"checked\";a:1:{s:8:\"quotient\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(251, 'gf_previous_db_version', '2.2.5.5', 'yes'),
(252, 'gf_upgrade_lock', '', 'yes'),
(253, 'gform_sticky_admin_messages', 'a:0:{}', 'yes'),
(126, 'can_compress_scripts', '0', 'no');
INSERT INTO `wpqd_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(258, 'gform_version_info', 'a:10:{s:12:\"is_valid_key\";b:1;s:6:\"reason\";s:0:\"\";s:7:\"version\";s:5:\"2.4.9\";s:3:\"url\";s:164:\"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=ot4SUYmGmRSgPz7iRnbgjQpvzY4%3D\";s:15:\"expiration_time\";i:1588348800;s:9:\"offerings\";a:52:{s:12:\"gravityforms\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:5:\"2.4.9\";s:14:\"version_latest\";s:5:\"2.4.9\";s:3:\"url\";s:164:\"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=ot4SUYmGmRSgPz7iRnbgjQpvzY4%3D\";s:10:\"url_latest\";s:164:\"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=ot4SUYmGmRSgPz7iRnbgjQpvzY4%3D\";}s:21:\"gravityforms2checkout\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:3:\"1.0\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/2checkout/gravityforms2checkout_1.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=cxBOZe5wGx7ZuSTLRn0OtiRfyRY%3D\";s:10:\"url_latest\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/2checkout/gravityforms2checkout_1.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=cxBOZe5wGx7ZuSTLRn0OtiRfyRY%3D\";}s:26:\"gravityformsactivecampaign\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.6\";s:14:\"version_latest\";s:3:\"1.6\";s:3:\"url\";s:191:\"http://s3.amazonaws.com/gravityforms/addons/activecampaign/gravityformsactivecampaign_1.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=yYstLEmvSvA%2FTjsem7XQasnP0mA%3D\";s:10:\"url_latest\";s:191:\"http://s3.amazonaws.com/gravityforms/addons/activecampaign/gravityformsactivecampaign_1.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=yYstLEmvSvA%2FTjsem7XQasnP0mA%3D\";}s:32:\"gravityformsadvancedpostcreation\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"1.0-beta-2\";s:14:\"version_latest\";s:12:\"1.0-beta-2.2\";s:3:\"url\";s:210:\"http://s3.amazonaws.com/gravityforms/addons/advancedpostcreation/gravityformsadvancedpostcreation_1.0-beta-2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=9mZ7AUzB0kZDqs3fzO0%2BZF2YLZk%3D\";s:10:\"url_latest\";s:210:\"http://s3.amazonaws.com/gravityforms/addons/advancedpostcreation/gravityformsadvancedpostcreation_1.0-beta-2.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=mzWJuiZJaVKKfA2xeiwJjRJbJ74%3D\";}s:20:\"gravityformsagilecrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/agilecrm/gravityformsagilecrm_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=d%2FFKdzIKWKe2afDqAhA0E7DE8ZM%3D\";s:10:\"url_latest\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/agilecrm/gravityformsagilecrm_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=d%2FFKdzIKWKe2afDqAhA0E7DE8ZM%3D\";}s:24:\"gravityformsauthorizenet\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.6\";s:14:\"version_latest\";s:3:\"2.6\";s:3:\"url\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/authorizenet/gravityformsauthorizenet_2.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=EB4rkGA95Y6uwDs395RbYWRMz9c%3D\";s:10:\"url_latest\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/authorizenet/gravityformsauthorizenet_2.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=EB4rkGA95Y6uwDs395RbYWRMz9c%3D\";}s:18:\"gravityformsaweber\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.8\";s:14:\"version_latest\";s:3:\"2.8\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/aweber/gravityformsaweber_2.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=eBMKnLPwEiGDgFhhYn2dQbuazo4%3D\";s:10:\"url_latest\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/aweber/gravityformsaweber_2.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=eBMKnLPwEiGDgFhhYn2dQbuazo4%3D\";}s:21:\"gravityformsbatchbook\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/batchbook/gravityformsbatchbook_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=YzNqu08Mps%2FTHo5%2FyGMA7BunF%2FE%3D\";s:10:\"url_latest\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/batchbook/gravityformsbatchbook_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=YzNqu08Mps%2FTHo5%2FyGMA7BunF%2FE%3D\";}s:18:\"gravityformsbreeze\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/breeze/gravityformsbreeze_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=oHxBLMKu6Vl43fpEecLOqOks81Q%3D\";s:10:\"url_latest\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/breeze/gravityformsbreeze_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=oHxBLMKu6Vl43fpEecLOqOks81Q%3D\";}s:27:\"gravityformscampaignmonitor\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.7\";s:14:\"version_latest\";s:3:\"3.7\";s:3:\"url\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/campaignmonitor/gravityformscampaignmonitor_3.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=%2BjmTiGR4r6WnnUXBI2wFdhgn7mQ%3D\";s:10:\"url_latest\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/campaignmonitor/gravityformscampaignmonitor_3.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=%2BjmTiGR4r6WnnUXBI2wFdhgn7mQ%3D\";}s:20:\"gravityformscampfire\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.2.2\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/campfire/gravityformscampfire_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=RcLzfdPSUdS8CaKTZdBtw%2BcmVy4%3D\";s:10:\"url_latest\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/campfire/gravityformscampfire_1.2.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=TIxJv2y1KWjAC0AkmJg6EXpmAdg%3D\";}s:22:\"gravityformscapsulecrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.1\";s:3:\"url\";s:187:\"http://s3.amazonaws.com/gravityforms/addons/capsulecrm/gravityformscapsulecrm_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=Z1%2FCZ3FOTqRR5Hdn%2BMHJ%2FkHTsn4%3D\";s:10:\"url_latest\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/capsulecrm/gravityformscapsulecrm_1.3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=YZrkcX65IBDVGOHUu3pCsCp%2F22s%3D\";}s:26:\"gravityformschainedselects\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.6\";s:3:\"url\";s:191:\"http://s3.amazonaws.com/gravityforms/addons/chainedselects/gravityformschainedselects_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=Kgfat7Z%2BQRGKK7Spj9p64a4fgjo%3D\";s:10:\"url_latest\";s:191:\"http://s3.amazonaws.com/gravityforms/addons/chainedselects/gravityformschainedselects_1.1.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=Ghb4gZcpzY6fndCib4wzZdDhaLw%3D\";}s:23:\"gravityformscleverreach\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:5:\"1.5.1\";s:3:\"url\";s:187:\"http://s3.amazonaws.com/gravityforms/addons/cleverreach/gravityformscleverreach_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=Yc%2BxmSpoASdTnv98i7IOuV%2FdYQQ%3D\";s:10:\"url_latest\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/cleverreach/gravityformscleverreach_1.5.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=C8E7aFmqAiI2Jxi4Qw8yNX9p8UQ%3D\";}s:27:\"gravityformsconstantcontact\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:3:\"1.0\";s:3:\"url\";s:191:\"http://s3.amazonaws.com/gravityforms/addons/constantcontact/gravityformsconstantcontact_1.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=0wLNkUiQDJCoI21muzuBoDTLaZE%3D\";s:10:\"url_latest\";s:191:\"http://s3.amazonaws.com/gravityforms/addons/constantcontact/gravityformsconstantcontact_1.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=0wLNkUiQDJCoI21muzuBoDTLaZE%3D\";}s:19:\"gravityformscoupons\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.8\";s:14:\"version_latest\";s:5:\"2.8.3\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/coupons/gravityformscoupons_2.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=I10vJabnZCYIz3haolo8p04PslU%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/coupons/gravityformscoupons_2.8.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=MQxp3xXbb287doMhxbN0ZlBur4Y%3D\";}s:17:\"gravityformsdebug\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";s:10:\"1.0.beta11\";s:3:\"url\";s:0:\"\";s:10:\"url_latest\";s:178:\"http://s3.amazonaws.com/gravityforms/addons/debug/gravityformsdebug_1.0.beta11.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=URcp3u1p3ptKZanteiaWkQFO7sI%3D\";}s:19:\"gravityformsdropbox\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.3\";s:14:\"version_latest\";s:3:\"2.3\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/dropbox/gravityformsdropbox_2.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=O2MysWLQdV7VF4aP47TdqDFjICU%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/dropbox/gravityformsdropbox_2.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=O2MysWLQdV7VF4aP47TdqDFjICU%3D\";}s:16:\"gravityformsemma\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.5\";s:3:\"url\";s:169:\"http://s3.amazonaws.com/gravityforms/addons/emma/gravityformsemma_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=mxbv8I9WpXQeGInFvFZyWC9lQlc%3D\";s:10:\"url_latest\";s:171:\"http://s3.amazonaws.com/gravityforms/addons/emma/gravityformsemma_1.2.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=CD4PkTQ2ogQGWGDlq6GJS4WZCnY%3D\";}s:22:\"gravityformsfreshbooks\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.5\";s:14:\"version_latest\";s:5:\"2.5.2\";s:3:\"url\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/freshbooks/gravityformsfreshbooks_2.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=fSba2boudbMgLA4rRNiRSl041jQ%3D\";s:10:\"url_latest\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/freshbooks/gravityformsfreshbooks_2.5.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=hphqJR%2BUasGAlgwKx0EjJYnvF3U%3D\";}s:23:\"gravityformsgetresponse\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:183:\"http://s3.amazonaws.com/gravityforms/addons/getresponse/gravityformsgetresponse_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=JXuXSRXwBrpSDtE56t4EMSaMamY%3D\";s:10:\"url_latest\";s:183:\"http://s3.amazonaws.com/gravityforms/addons/getresponse/gravityformsgetresponse_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=JXuXSRXwBrpSDtE56t4EMSaMamY%3D\";}s:21:\"gravityformsgutenberg\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"1.0-rc-1.2\";s:14:\"version_latest\";s:10:\"1.0-rc-1.2\";s:3:\"url\";s:190:\"http://s3.amazonaws.com/gravityforms/addons/gutenberg/gravityformsgutenberg_1.0-rc-1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=%2B%2BShvld2vujR3Da2KoToua26yyE%3D\";s:10:\"url_latest\";s:190:\"http://s3.amazonaws.com/gravityforms/addons/gutenberg/gravityformsgutenberg_1.0-rc-1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=%2B%2BShvld2vujR3Da2KoToua26yyE%3D\";}s:21:\"gravityformshelpscout\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:5:\"1.5.3\";s:3:\"url\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/helpscout/gravityformshelpscout_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=Hv%2Fi33eVmfgcuIpIZqjEgn03KzQ%3D\";s:10:\"url_latest\";s:183:\"http://s3.amazonaws.com/gravityforms/addons/helpscout/gravityformshelpscout_1.5.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=wMR9Eqxl%2Bi16AoaDekQIP0QBQVA%3D\";}s:20:\"gravityformshighrise\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/highrise/gravityformshighrise_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=WnSuCG2ujpxcGvjzdv0V4phKFHk%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/highrise/gravityformshighrise_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=WnSuCG2ujpxcGvjzdv0V4phKFHk%3D\";}s:19:\"gravityformshipchat\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/hipchat/gravityformshipchat_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=k%2FEZNJVXsFlN8slg9Q%2FUQI%2BDzkk%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/hipchat/gravityformshipchat_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=k%2FEZNJVXsFlN8slg9Q%2FUQI%2BDzkk%3D\";}s:20:\"gravityformsicontact\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.1\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/icontact/gravityformsicontact_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=U%2B6GpUPDoJ1a6ItRqW1fT4Kj4FE%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/icontact/gravityformsicontact_1.3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=MeqJHkcWUGG%2BDCRxGBxY1ew6d58%3D\";}s:19:\"gravityformslogging\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.1\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/logging/gravityformslogging_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=2Y%2BOpiRGlfGDvNOd33vnIa77WkM%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/logging/gravityformslogging_1.3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=FvP2DnCv3ihdtdq%2FafIHQaAeC%2BU%3D\";}s:19:\"gravityformsmadmimi\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/madmimi/gravityformsmadmimi_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=CS%2Fr2CfXob1t6pRrRks2F%2F2rgjM%3D\";s:10:\"url_latest\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/madmimi/gravityformsmadmimi_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=CS%2Fr2CfXob1t6pRrRks2F%2F2rgjM%3D\";}s:21:\"gravityformsmailchimp\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"4.6\";s:14:\"version_latest\";s:3:\"4.6\";s:3:\"url\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/mailchimp/gravityformsmailchimp_4.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=fPxw%2F6jfu0ewfGrL9AsHXYZQ2Fo%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/mailchimp/gravityformsmailchimp_4.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=fPxw%2F6jfu0ewfGrL9AsHXYZQ2Fo%3D\";}s:19:\"gravityformsmailgun\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.2\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/mailgun/gravityformsmailgun_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=nnT%2FzC4f42Awg%2FIYUhOGRFd1SvU%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/mailgun/gravityformsmailgun_1.1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=H0tUzPfgb4prtXYV4mnjvdzjiTg%3D\";}s:26:\"gravityformspartialentries\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.2\";s:3:\"url\";s:189:\"http://s3.amazonaws.com/gravityforms/addons/partialentries/gravityformspartialentries_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=Ziwcn0a2Cb7begf1hNBzjpGvcNs%3D\";s:10:\"url_latest\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/partialentries/gravityformspartialentries_1.3.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=epphgWJwt3t%2BWlSfYNXbw6BdG8g%3D\";}s:18:\"gravityformspaypal\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.1\";s:14:\"version_latest\";s:5:\"3.1.1\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/paypal/gravityformspaypal_3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=gIb1Kc1mFz0aIKqtHSES8kwti4c%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/paypal/gravityformspaypal_3.1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=JMw2XQDi%2Fmr1CE6aeI7kTc6laBY%3D\";}s:33:\"gravityformspaypalexpresscheckout\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";N;}s:29:\"gravityformspaypalpaymentspro\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.3\";s:14:\"version_latest\";s:5:\"2.3.3\";s:3:\"url\";s:195:\"http://s3.amazonaws.com/gravityforms/addons/paypalpaymentspro/gravityformspaypalpaymentspro_2.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=9CdlIOCDcpEWVH2JqAFl6T8C5DE%3D\";s:10:\"url_latest\";s:197:\"http://s3.amazonaws.com/gravityforms/addons/paypalpaymentspro/gravityformspaypalpaymentspro_2.3.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=mVVMGX2h3b95DPfnK8KaQjb7OQk%3D\";}s:21:\"gravityformspaypalpro\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:5:\"1.8.1\";s:14:\"version_latest\";s:5:\"1.8.1\";s:3:\"url\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/paypalpro/gravityformspaypalpro_1.8.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=8usU8uvEm1Q7nVD0YzuwyFWN7hY%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/paypalpro/gravityformspaypalpro_1.8.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=8usU8uvEm1Q7nVD0YzuwyFWN7hY%3D\";}s:20:\"gravityformspicatcha\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:3:\"2.0\";}s:16:\"gravityformspipe\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.1\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/pipe/gravityformspipe_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=o%2BtKHT%2BME%2FaFVsSkLXiNTH3JMcs%3D\";s:10:\"url_latest\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/pipe/gravityformspipe_1.1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=mhv0ysFePyZEpgue%2BtAoc7PTTyk%3D\";}s:17:\"gravityformspolls\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.2\";s:14:\"version_latest\";s:5:\"3.2.2\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/polls/gravityformspolls_3.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=jY85CCHVjXWsOaYoKFpe%2FKfw9vs%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/polls/gravityformspolls_3.2.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=J5K7UnjpF47BQvWaRonxRNKTF%2BA%3D\";}s:20:\"gravityformspostmark\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:5:\"1.0.1\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/postmark/gravityformspostmark_1.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=0dlipJSGDxpV2lqkgPCmYZYLenE%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/postmark/gravityformspostmark_1.0.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=PKxHb1uFROTuV3R4N0E5Ug%2B34pI%3D\";}s:16:\"gravityformsquiz\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.1\";s:14:\"version_latest\";s:5:\"3.1.9\";s:3:\"url\";s:171:\"http://s3.amazonaws.com/gravityforms/addons/quiz/gravityformsquiz_3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=PwXLEd075W5n3AyxPTtR%2FyNF70c%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/quiz/gravityformsquiz_3.1.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=RlFAr5QQXxf4DojEdz%2BZr1F6B%2Fg%3D\";}s:19:\"gravityformsrestapi\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"2.0-beta-2\";s:14:\"version_latest\";s:10:\"2.0-beta-2\";s:3:\"url\";s:184:\"http://s3.amazonaws.com/gravityforms/addons/restapi/gravityformsrestapi_2.0-beta-2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=mwOnKVgORJIpaLefnmf4bGkl1%2B0%3D\";s:10:\"url_latest\";s:184:\"http://s3.amazonaws.com/gravityforms/addons/restapi/gravityformsrestapi_2.0-beta-2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=mwOnKVgORJIpaLefnmf4bGkl1%2B0%3D\";}s:20:\"gravityformssendgrid\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.1\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/sendgrid/gravityformssendgrid_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=FqCMDMgm1r3y6iy6LP%2FkhDV1e0k%3D\";s:10:\"url_latest\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/sendgrid/gravityformssendgrid_1.2.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=J9YVjWYC76r93SeDEkWhRyyRB4Q%3D\";}s:21:\"gravityformssignature\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.6\";s:14:\"version_latest\";s:5:\"3.7.1\";s:3:\"url\";s:183:\"http://s3.amazonaws.com/gravityforms/addons/signature/gravityformssignature_3.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=OPGhP%2Fzj%2Flmt8moa4ygqJUk2uRk%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/signature/gravityformssignature_3.7.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=FHL2ejAGgQTAdjxtpWDoRsbHHc8%3D\";}s:17:\"gravityformsslack\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.8\";s:14:\"version_latest\";s:3:\"1.8\";s:3:\"url\";s:171:\"http://s3.amazonaws.com/gravityforms/addons/slack/gravityformsslack_1.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=iG021hu7cDLKIRlN7kvK80BvOHc%3D\";s:10:\"url_latest\";s:171:\"http://s3.amazonaws.com/gravityforms/addons/slack/gravityformsslack_1.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=iG021hu7cDLKIRlN7kvK80BvOHc%3D\";}s:18:\"gravityformsstripe\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.7\";s:14:\"version_latest\";s:3:\"2.7\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/stripe/gravityformsstripe_2.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=RdE7jbzpUv0tMMr%2F7pvyBfiGQZY%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/stripe/gravityformsstripe_2.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=RdE7jbzpUv0tMMr%2F7pvyBfiGQZY%3D\";}s:18:\"gravityformssurvey\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.3\";s:14:\"version_latest\";s:5:\"3.3.1\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/survey/gravityformssurvey_3.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=t5IU2Q6j2hjq9fDYnH%2B49Gmuh6M%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/survey/gravityformssurvey_3.3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=ScPkKlOsG5FRrGyhqudvGArQ2vI%3D\";}s:18:\"gravityformstrello\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.2\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/trello/gravityformstrello_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=26v2Iy%2BztELGSezAHbE8QyUiPLE%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/trello/gravityformstrello_1.2.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=%2BMe1LOfTa%2FiOfFCnDtS%2BuH7xDhw%3D\";}s:18:\"gravityformstwilio\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.5\";s:14:\"version_latest\";s:5:\"2.5.2\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/twilio/gravityformstwilio_2.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=42aGkJd4qz496n5CoGroup4ucBM%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/twilio/gravityformstwilio_2.5.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=IrBxFbv2xuqHrD4TM1RjOSgSjuk%3D\";}s:28:\"gravityformsuserregistration\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"4.0\";s:14:\"version_latest\";s:5:\"4.0.9\";s:3:\"url\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/userregistration/gravityformsuserregistration_4.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=vRG6zRwTNc2ISYlPu0C5CfBhSi4%3D\";s:10:\"url_latest\";s:197:\"http://s3.amazonaws.com/gravityforms/addons/userregistration/gravityformsuserregistration_4.0.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=JT8UCdw1FM5WKahXW2LC%2BPQDSHI%3D\";}s:20:\"gravityformswebhooks\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/webhooks/gravityformswebhooks_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=yZy7ETt646K8GtkaNixCzByrzxM%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/webhooks/gravityformswebhooks_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=yZy7ETt646K8GtkaNixCzByrzxM%3D\";}s:18:\"gravityformszapier\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.1\";s:14:\"version_latest\";s:5:\"3.1.4\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/zapier/gravityformszapier_3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=0Pw1YLxVFdYksxl8QVjgEjStb6s%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/zapier/gravityformszapier_3.1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=6FbZckMdUThPXhO9hhmfDN4LWxc%3D\";}s:19:\"gravityformszohocrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.7\";s:14:\"version_latest\";s:5:\"1.7.3\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/zohocrm/gravityformszohocrm_1.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=%2BeA2GpFKqiLBgJIjUsyovjTXegQ%3D\";s:10:\"url_latest\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/zohocrm/gravityformszohocrm_1.7.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=a1m1qW1Wp%2FXcDHpoZPzl7xTUfqc%3D\";}}s:9:\"is_active\";s:1:\"1\";s:14:\"version_latest\";s:5:\"2.4.9\";s:10:\"url_latest\";s:164:\"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1558064213&Signature=ot4SUYmGmRSgPz7iRnbgjQpvzY4%3D\";s:9:\"timestamp\";i:1557891413;}', 'yes'),
(257, 'gf_submissions_block', '', 'yes'),
(221, 'gravityformsaddon_gravityformswebapi_settings', 'a:4:{s:7:\"enabled\";s:1:\"1\";s:10:\"public_key\";s:10:\"85917af7f6\";s:11:\"private_key\";s:15:\"e46441357e29ccb\";s:19:\"impersonate_account\";s:1:\"1\";}', 'yes'),
(226, 'gform_api_count', '57', 'yes'),
(712, '_transient_timeout_easy_wp_smtp_sd_code', '1557871208', 'no'),
(713, '_transient_easy_wp_smtp_sd_code', '3b3bbc330f53f061a7bf45e1a839dda1', 'no'),
(714, 'swpsmtp_options', 'a:8:{s:16:\"from_email_field\";s:21:\"admin@adsquotient.com\";s:15:\"from_name_field\";s:11:\"adsquotient\";s:23:\"force_from_name_replace\";b:0;s:13:\"smtp_settings\";a:9:{s:4:\"host\";s:14:\"smtp.gmail.com\";s:15:\"type_encryption\";s:3:\"ssl\";s:4:\"port\";s:3:\"465\";s:13:\"autentication\";s:3:\"yes\";s:8:\"username\";s:24:\"velmurugan@socialbeat.in\";s:8:\"password\";s:16:\"dmVsQHNhcm9qYQ==\";s:12:\"enable_debug\";b:0;s:12:\"insecure_ssl\";b:0;s:12:\"encrypt_pass\";b:0;}s:15:\"allowed_domains\";s:28:\"d3d3LmFkc3F1b3RpZW50LmNvbQ==\";s:14:\"reply_to_email\";s:0:\"\";s:17:\"email_ignore_list\";s:0:\"\";s:19:\"enable_domain_check\";b:0;}', 'yes'),
(715, 'smtp_test_mail', 'a:3:{s:10:\"swpsmtp_to\";s:21:\"shalini@socialbeat.in\";s:15:\"swpsmtp_subject\";s:11:\"testing ads\";s:15:\"swpsmtp_message\";s:6:\"tesing\";}', 'yes'),
(250, 'gf_rest_api_db_version', '2.4.8', 'yes'),
(139, 'current_theme', 'Quotient', 'yes'),
(140, 'theme_mods_quotient', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(146, 'recently_activated', 'a:1:{s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";i:1557561976;}', 'yes'),
(147, 'widget_gform_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(148, 'gravityformsaddon_gravityformswebapi_version', '1.0', 'yes'),
(149, 'gform_enable_background_updates', '1', 'yes'),
(150, 'gf_db_version', '2.4.8', 'no'),
(151, 'gform_pending_installation', '', 'yes'),
(152, 'rg_form_version', '2.4.8', 'yes'),
(166, 'rg_gforms_currency', '', 'yes'),
(164, 'gform_enable_noconflict', '1', 'yes'),
(165, 'rg_gforms_enable_akismet', '1', 'yes'),
(163, 'rg_gforms_key', '5bb5d303652795f89ccb0bc7eeddff7b', 'yes'),
(173, 'category_children', 'a:0:{}', 'yes'),
(572, '_site_transient_timeout_browser_53ad83e7ffe60968becca0ade7b723b5', '1558166329', 'no'),
(573, '_site_transient_browser_53ad83e7ffe60968becca0ade7b723b5', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"74.0.3729.131\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(574, '_site_transient_timeout_php_check_f114d5fab08d132d0e447a6ad732d25d', '1558166330', 'no'),
(575, '_site_transient_php_check_f114d5fab08d132d0e447a6ad732d25d', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(594, 'rlrsssl_options', 'a:15:{s:12:\"site_has_ssl\";b:0;s:25:\"ssl_success_message_shown\";b:0;s:22:\"htaccess_warning_shown\";b:0;s:19:\"review_notice_shown\";b:0;s:17:\"plugin_db_version\";s:5:\"3.1.5\";s:11:\"ssl_enabled\";b:0;s:9:\"debug_log\";N;s:4:\"hsts\";b:0;s:19:\"javascript_redirect\";b:0;s:11:\"wp_redirect\";b:0;s:26:\"autoreplace_insecure_links\";b:1;s:5:\"debug\";b:0;s:20:\"do_not_edit_htaccess\";b:0;s:31:\"switch_mixed_content_fixer_hook\";b:0;s:17:\"htaccess_redirect\";b:0;}', 'yes'),
(600, '_site_transient_timeout_browser_0cd24212be0d014e8bf6ddb43d7643c9', '1558177706', 'no'),
(601, '_site_transient_browser_0cd24212be0d014e8bf6ddb43d7643c9', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"74.0.3729.131\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(606, 'gform_email_count', '3', 'yes'),
(697, 'gf_dismissed_upgrades', 'a:1:{i:0;s:5:\"2.4.9\";}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_postmeta`
--

CREATE TABLE `wpqd_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wpqd_postmeta`
--

INSERT INTO `wpqd_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(6, 6, '_wp_page_template', 'ads.php'),
(5, 6, '_edit_lock', '1556812973:1'),
(7, 8, '_edit_lock', '1556882678:1'),
(8, 8, '_wp_page_template', 'thankyou.php'),
(10, 11, '_edit_lock', '1557146871:1'),
(11, 11, '_wp_page_template', 'privacy.php'),
(12, 3, '_wp_trash_meta_status', 'draft'),
(13, 3, '_wp_trash_meta_time', '1557146764'),
(14, 3, '_wp_desired_post_slug', 'privacy-policy');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_posts`
--

CREATE TABLE `wpqd_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wpqd_posts`
--

INSERT INTO `wpqd_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-05-02 15:35:27', '2019-05-02 15:35:27', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-05-02 15:35:27', '2019-05-02 15:35:27', '', 0, 'http://www.adsquotient.com/?p=1', 0, 'post', '', 1),
(2, 1, '2019-05-02 15:35:27', '2019-05-02 15:35:27', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://www.adsquotient.com/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-05-02 15:35:27', '2019-05-02 15:35:27', '', 0, 'http://www.adsquotient.com/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-05-02 15:35:27', '2019-05-02 15:35:27', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://www.adsquotient.com.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'trash', 'closed', 'open', '', 'privacy-policy__trashed', '', '', '2019-05-06 12:46:04', '2019-05-06 12:46:04', '', 0, 'http://www.adsquotient.com/?page_id=3', 0, 'page', '', 0),
(17, 1, '2019-05-11 07:58:50', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-05-11 07:58:50', '0000-00-00 00:00:00', '', 0, 'http://www.adsquotient.com/?p=17', 0, 'post', '', 0),
(6, 1, '2019-05-02 16:04:53', '2019-05-02 16:04:53', '', 'Homepage', '', 'publish', 'closed', 'closed', '', 'homepage', '', '', '2019-05-02 16:04:53', '2019-05-02 16:04:53', '', 0, 'http://www.adsquotient.com/?page_id=6', 0, 'page', '', 0),
(7, 1, '2019-05-02 16:04:53', '2019-05-02 16:04:53', '', 'Homepage', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-05-02 16:04:53', '2019-05-02 16:04:53', '', 6, 'http://www.adsquotient.com/2019/05/02/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2019-05-03 05:20:57', '2019-05-03 05:20:57', '', 'Thankyou', '', 'publish', 'closed', 'closed', '', 'thankyou', '', '', '2019-05-03 05:20:57', '2019-05-03 05:20:57', '', 0, 'http://www.adsquotient.com/?page_id=8', 0, 'page', '', 0),
(9, 1, '2019-05-03 05:20:57', '2019-05-03 05:20:57', '', 'Thankyou', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2019-05-03 05:20:57', '2019-05-03 05:20:57', '', 8, 'http://www.adsquotient.com/2019/05/03/8-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2019-05-06 11:05:43', '2019-05-06 11:05:43', '', 'Privacy Policy', '', 'publish', 'closed', 'closed', '', 'privacy-policy', '', '', '2019-05-06 12:47:16', '2019-05-06 12:47:16', '', 0, 'http://www.adsquotient.com/?page_id=11', 0, 'page', '', 0),
(12, 1, '2019-05-06 11:05:43', '2019-05-06 11:05:43', '', 'Privacy', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-05-06 11:05:43', '2019-05-06 11:05:43', '', 11, 'http://www.adsquotient.com/2019/05/06/11-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2019-05-06 12:47:05', '2019-05-06 12:47:05', '', 'Privacy', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-05-06 12:47:05', '2019-05-06 12:47:05', '', 11, 'http://www.adsquotient.com/2019/05/06/11-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2019-05-06 12:45:37', '2019-05-06 12:45:37', '', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-05-06 12:45:37', '2019-05-06 12:45:37', '', 11, 'http://www.adsquotient.com/2019/05/06/11-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2019-05-06 12:46:04', '2019-05-06 12:46:04', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://www.adsquotient.com.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2019-05-06 12:46:04', '2019-05-06 12:46:04', '', 3, 'http://www.adsquotient.com/2019/05/06/3-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2019-05-06 12:47:16', '2019-05-06 12:47:16', '', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-05-06 12:47:16', '2019-05-06 12:47:16', '', 11, 'http://www.adsquotient.com/2019/05/06/11-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_rg_form`
--

CREATE TABLE `wpqd_rg_form` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_trash` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wpqd_rg_form`
--

INSERT INTO `wpqd_rg_form` (`id`, `title`, `date_created`, `is_active`, `is_trash`) VALUES
(1, 'Enquire now', '2019-05-02 15:59:56', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_rg_form_meta`
--

CREATE TABLE `wpqd_rg_form_meta` (
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `entries_grid_meta` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `confirmations` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `notifications` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wpqd_rg_form_meta`
--

INSERT INTO `wpqd_rg_form_meta` (`form_id`, `display_meta`, `entries_grid_meta`, `confirmations`, `notifications`) VALUES
(1, '{\"title\":\"Enquire now\",\"description\":\"\",\"labelPlacement\":\"top_label\",\"descriptionPlacement\":\"below\",\"button\":{\"type\":\"text\",\"text\":\"Submit\",\"imageUrl\":\"\"},\"fields\":[{\"type\":\"text\",\"id\":1,\"label\":\"\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"Name\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1},{\"type\":\"text\",\"id\":2,\"label\":\"\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"Email\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1},{\"type\":\"text\",\"id\":3,\"label\":\"\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"Phone\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1},{\"type\":\"hidden\",\"id\":4,\"label\":\"URL\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1}],\"version\":\"2.2.5.5\",\"id\":1,\"useCurrentUserAsAuthor\":true,\"postContentTemplateEnabled\":false,\"postTitleTemplateEnabled\":false,\"postTitleTemplate\":\"\",\"postContentTemplate\":\"\",\"lastPageButton\":null,\"pagination\":null,\"firstPageCssClass\":null}', NULL, '{\"5ccb13fc168bd\":{\"id\":\"5ccb13fc168bd\",\"name\":\"Default Confirmation\",\"isDefault\":true,\"type\":\"message\",\"message\":\"Thanks for contacting us! We will get in touch with you shortly.\",\"url\":\"\",\"pageId\":\"\",\"queryString\":\"\"}}', '{\"5ccb13fc0fb8f\":{\"id\":\"5ccb13fc0fb8f\",\"to\":\"{admin_email}\",\"name\":\"Admin Notification\",\"event\":\"form_submission\",\"toType\":\"email\",\"subject\":\"New submission from {form_title}\",\"message\":\"{all_fields}\"}}');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_rg_form_view`
--

CREATE TABLE `wpqd_rg_form_view` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `count` mediumint(8) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_rg_incomplete_submissions`
--

CREATE TABLE `wpqd_rg_incomplete_submissions` (
  `uuid` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `submission` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_rg_lead`
--

CREATE TABLE `wpqd_rg_lead` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT 0,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_agent` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `currency` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_status` varchar(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `transaction_id` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'active'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wpqd_rg_lead`
--

INSERT INTO `wpqd_rg_lead` (`id`, `form_id`, `post_id`, `date_created`, `is_starred`, `is_read`, `ip`, `source_url`, `user_agent`, `currency`, `payment_status`, `payment_date`, `payment_amount`, `payment_method`, `transaction_id`, `is_fulfilled`, `created_by`, `transaction_type`, `status`) VALUES
(1, 1, NULL, '2019-05-03 05:24:45', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(2, 1, NULL, '2019-05-03 05:31:27', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(3, 1, NULL, '2019-05-03 06:11:10', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(4, 1, NULL, '2019-05-03 07:42:23', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(5, 1, NULL, '2019-05-03 09:03:30', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(6, 1, NULL, '2019-05-03 09:16:57', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active'),
(7, 1, NULL, '2019-05-03 10:18:24', 0, 0, '::1', 'http://www.adsquotient.com/', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', 'USD', NULL, NULL, NULL, '', NULL, NULL, 1, NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_rg_lead_detail`
--

CREATE TABLE `wpqd_rg_lead_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lead_id` int(10) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `field_number` float NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wpqd_rg_lead_detail`
--

INSERT INTO `wpqd_rg_lead_detail` (`id`, `lead_id`, `form_id`, `field_number`, `value`) VALUES
(1, 1, 1, 1, 'test SB'),
(2, 1, 1, 2, 'xv@dfg.df'),
(3, 1, 1, 3, '5676767575'),
(4, 1, 1, 4, 'http://www.adsquotient.com/'),
(5, 2, 1, 1, 'test'),
(6, 2, 1, 2, 'test@gmail.com'),
(7, 2, 1, 3, '1234567890'),
(8, 2, 1, 4, 'http://www.adsquotient.com/'),
(9, 3, 1, 1, 'test SB'),
(10, 3, 1, 2, 'test@gmail.com'),
(11, 3, 1, 3, '9898888886'),
(12, 3, 1, 4, 'http://www.adsquotient.com/'),
(13, 4, 1, 1, 'test'),
(14, 4, 1, 2, 'test@gmail.com'),
(15, 4, 1, 3, '1212121212'),
(16, 4, 1, 4, 'http://www.adsquotient.com/'),
(17, 5, 1, 1, 'Ejdj'),
(18, 5, 1, 2, 'djjd@hsjs.dnd'),
(19, 5, 1, 3, '9898988787'),
(20, 5, 1, 4, 'http://www.adsquotient.com/'),
(21, 6, 1, 1, 'test SB'),
(22, 6, 1, 2, 'test@gmail.com'),
(23, 6, 1, 3, '9898989898'),
(24, 6, 1, 4, 'http://www.adsquotient.com/'),
(25, 7, 1, 1, 'test SB'),
(26, 7, 1, 2, 'test@gmail.com'),
(27, 7, 1, 3, '9898989890'),
(28, 7, 1, 4, 'http://www.adsquotient.com/');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_rg_lead_detail_long`
--

CREATE TABLE `wpqd_rg_lead_detail_long` (
  `lead_detail_id` bigint(20) UNSIGNED NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_rg_lead_meta`
--

CREATE TABLE `wpqd_rg_lead_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `lead_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_rg_lead_notes`
--

CREATE TABLE `wpqd_rg_lead_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `lead_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `note_type` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_termmeta`
--

CREATE TABLE `wpqd_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_terms`
--

CREATE TABLE `wpqd_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wpqd_terms`
--

INSERT INTO `wpqd_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_term_relationships`
--

CREATE TABLE `wpqd_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wpqd_term_relationships`
--

INSERT INTO `wpqd_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_term_taxonomy`
--

CREATE TABLE `wpqd_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wpqd_term_taxonomy`
--

INSERT INTO `wpqd_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_usermeta`
--

CREATE TABLE `wpqd_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wpqd_usermeta`
--

INSERT INTO `wpqd_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wpqd_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wpqd_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '0'),
(22, 1, 'session_tokens', 'a:1:{s:64:\"f7476b18144ece6598d2017c824614f65b8c56a248448faa20093bdf4054cc33\";a:4:{s:10:\"expiration\";i:1557928632;s:2:\"ip\";s:12:\"45.251.35.45\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36\";s:5:\"login\";i:1557755832;}}'),
(23, 1, '_new_email', 'a:2:{s:4:\"hash\";s:32:\"a1eb372589aac72c76f995395055db73\";s:8:\"newemail\";s:21:\"admin@adsquotient.com\";}'),
(17, 1, 'wpqd_dashboard_quick_press_last_post_id', '17'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"45.251.35.0\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wpqd_users`
--

CREATE TABLE `wpqd_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wpqd_users`
--

INSERT INTO `wpqd_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BUpL1L.hCYp225HbMOZKemmPJYIIVR/', 'admin', 'admin@beat.social', '', '2019-05-02 15:35:27', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_campaign_data`
--
ALTER TABLE `all_campaign_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benchmark_details`
--
ALTER TABLE `benchmark_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fb_campaign_details`
--
ALTER TABLE `fb_campaign_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fb_details`
--
ALTER TABLE `fb_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insights_codenames`
--
ALTER TABLE `insights_codenames`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insights_formulas`
--
ALTER TABLE `insights_formulas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wpqd_commentmeta`
--
ALTER TABLE `wpqd_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wpqd_comments`
--
ALTER TABLE `wpqd_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wpqd_gf_draft_submissions`
--
ALTER TABLE `wpqd_gf_draft_submissions`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wpqd_gf_entry`
--
ALTER TABLE `wpqd_gf_entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `form_id_status` (`form_id`,`status`);

--
-- Indexes for table `wpqd_gf_entry_meta`
--
ALTER TABLE `wpqd_gf_entry_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `meta_value` (`meta_value`(191));

--
-- Indexes for table `wpqd_gf_entry_notes`
--
ALTER TABLE `wpqd_gf_entry_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `entry_user_key` (`entry_id`,`user_id`);

--
-- Indexes for table `wpqd_gf_form`
--
ALTER TABLE `wpqd_gf_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wpqd_gf_form_meta`
--
ALTER TABLE `wpqd_gf_form_meta`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `wpqd_gf_form_revisions`
--
ALTER TABLE `wpqd_gf_form_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wpqd_gf_form_view`
--
ALTER TABLE `wpqd_gf_form_view`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wpqd_gf_rest_api_keys`
--
ALTER TABLE `wpqd_gf_rest_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wpqd_links`
--
ALTER TABLE `wpqd_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wpqd_options`
--
ALTER TABLE `wpqd_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wpqd_postmeta`
--
ALTER TABLE `wpqd_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wpqd_posts`
--
ALTER TABLE `wpqd_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wpqd_rg_form`
--
ALTER TABLE `wpqd_rg_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wpqd_rg_form_meta`
--
ALTER TABLE `wpqd_rg_form_meta`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `wpqd_rg_form_view`
--
ALTER TABLE `wpqd_rg_form_view`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wpqd_rg_incomplete_submissions`
--
ALTER TABLE `wpqd_rg_incomplete_submissions`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wpqd_rg_lead`
--
ALTER TABLE `wpqd_rg_lead`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `wpqd_rg_lead_detail`
--
ALTER TABLE `wpqd_rg_lead_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `lead_field_number` (`lead_id`,`field_number`),
  ADD KEY `lead_field_value` (`value`(191));

--
-- Indexes for table `wpqd_rg_lead_detail_long`
--
ALTER TABLE `wpqd_rg_lead_detail_long`
  ADD PRIMARY KEY (`lead_detail_id`);

--
-- Indexes for table `wpqd_rg_lead_meta`
--
ALTER TABLE `wpqd_rg_lead_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `form_id_meta_key` (`form_id`,`meta_key`(191));

--
-- Indexes for table `wpqd_rg_lead_notes`
--
ALTER TABLE `wpqd_rg_lead_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `lead_user_key` (`lead_id`,`user_id`);

--
-- Indexes for table `wpqd_termmeta`
--
ALTER TABLE `wpqd_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wpqd_terms`
--
ALTER TABLE `wpqd_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wpqd_term_relationships`
--
ALTER TABLE `wpqd_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wpqd_term_taxonomy`
--
ALTER TABLE `wpqd_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wpqd_usermeta`
--
ALTER TABLE `wpqd_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wpqd_users`
--
ALTER TABLE `wpqd_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `all_campaign_data`
--
ALTER TABLE `all_campaign_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `benchmark_details`
--
ALTER TABLE `benchmark_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `fb_details`
--
ALTER TABLE `fb_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `insights_codenames`
--
ALTER TABLE `insights_codenames`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `insights_formulas`
--
ALTER TABLE `insights_formulas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wpqd_commentmeta`
--
ALTER TABLE `wpqd_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_comments`
--
ALTER TABLE `wpqd_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wpqd_gf_entry`
--
ALTER TABLE `wpqd_gf_entry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `wpqd_gf_entry_meta`
--
ALTER TABLE `wpqd_gf_entry_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `wpqd_gf_entry_notes`
--
ALTER TABLE `wpqd_gf_entry_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_gf_form`
--
ALTER TABLE `wpqd_gf_form`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wpqd_gf_form_revisions`
--
ALTER TABLE `wpqd_gf_form_revisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_gf_form_view`
--
ALTER TABLE `wpqd_gf_form_view`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_gf_rest_api_keys`
--
ALTER TABLE `wpqd_gf_rest_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_links`
--
ALTER TABLE `wpqd_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_options`
--
ALTER TABLE `wpqd_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=793;

--
-- AUTO_INCREMENT for table `wpqd_postmeta`
--
ALTER TABLE `wpqd_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wpqd_posts`
--
ALTER TABLE `wpqd_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `wpqd_rg_form`
--
ALTER TABLE `wpqd_rg_form`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wpqd_rg_form_view`
--
ALTER TABLE `wpqd_rg_form_view`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_rg_lead`
--
ALTER TABLE `wpqd_rg_lead`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wpqd_rg_lead_detail`
--
ALTER TABLE `wpqd_rg_lead_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `wpqd_rg_lead_meta`
--
ALTER TABLE `wpqd_rg_lead_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_rg_lead_notes`
--
ALTER TABLE `wpqd_rg_lead_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_termmeta`
--
ALTER TABLE `wpqd_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpqd_terms`
--
ALTER TABLE `wpqd_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wpqd_term_taxonomy`
--
ALTER TABLE `wpqd_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wpqd_usermeta`
--
ALTER TABLE `wpqd_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `wpqd_users`
--
ALTER TABLE `wpqd_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
