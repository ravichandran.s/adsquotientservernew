<?php
function doGetFBAccIDFromCampaignID($mysqlLink, $aqID){
  $resultAry = array("success"=>false,"msg"=>'Inital error message',"data"=>array());
  $sql = "SELECT fb_details.fb_access_token,campaigns.fb_ad_account FROM fb_details JOIN campaigns ON fb_details.id_client = campaigns.client_id WHERE campaigns.id = ?";
  if($stmt = mysqli_prepare($mysqlLink, $sql)){
    /* bind variables to the prepared statement as parameters */
    mysqli_stmt_bind_param($stmt, "i", $aqID);
    /* execute query */
    mysqli_stmt_execute($stmt);
    mysqli_stmt_store_result($stmt);
    if(mysqli_stmt_num_rows($stmt)>0){
      /* bind result variables */
      mysqli_stmt_bind_result($stmt, $fbToken, $fbAccountID);
      /* fetch value */
      mysqli_stmt_fetch($stmt);
      $resultAry = array("success"=>true,"msg"=>"","data"=>array('fbtoken'=>$fbToken,'fbAccountID'=>$fbAccountID));
    }else{
      $resultAry = array("success"=>false,"msg"=>"No fb details found","data"=>array());
    }
  }else{
    $resultAry = array("success"=>false,"msg"=>"Failed to prepare statement","data"=>array());
  }
  return $resultAry;
}

function doGetFbAccessTokenFromCampaignId($mysqlLink, $clientID){
  $fbToken = '';
  $sql = "SELECT fb_access_token FROM fb_details WHERE id_client = '$clientID'";
  if($stmt = mysqli_prepare($mysqlLink, $sql)){
    mysqli_stmt_execute($stmt);
    mysqli_stmt_store_result($stmt);
    if(mysqli_stmt_num_rows($stmt)==1){
      /* bind result variables */
      mysqli_stmt_bind_result($stmt, $fbToken);
      /* fetch value */
      mysqli_stmt_fetch($stmt);
    }
  }    
  return $fbToken;
}

function doGetFbAdAccountNameCampaignId($mysqlLink, $aqID){
  $fbAdAccountName = '';
  $sql = "SELECT aqcampname, aqsector, aqcity FROM campaigns WHERE id = '$aqID'";
  if($stmt = mysqli_prepare($mysqlLink, $sql)){
    mysqli_stmt_execute($stmt);
    mysqli_stmt_store_result($stmt);
    if(mysqli_stmt_num_rows($stmt)==1){
      /* bind result variables */
      mysqli_stmt_bind_result($stmt, $fbAdAccountName, $aqSector ,$aqCity);
      /* fetch value */
      mysqli_stmt_fetch($stmt);
    }
  }    
  return array(
    'fbAdAccountName' => $fbAdAccountName,
    'fbSector' => $aqSector,
    'fbCity' => $aqCity
  );
}

function doFetchCampaignsData($mysqlLink, $aqID, $clientID, $FB, $dbfb_token){
  $today = date('Y-m-d');
  $aqCampResultAry = doGetCampIdFromAQId($mysqlLink, $aqID, $clientID);
  if($aqCampResultAry['success']){
    $dbaqCity = !empty($aqCampResultAry['data']['aqcity']) ? $aqCampResultAry['data']['aqcity'] : 'City not available';
    $dbInsights = !empty($aqCampResultAry['data']['aqinsights']) ? $aqCampResultAry['data']['aqinsights'] : 'Insights not available';
    $dbAqCampName = !empty($aqCampResultAry['data']['aqcampname']) ? $aqCampResultAry['data']['aqcampname'] : '';
    $dbAqFbAdAccountName  = !empty($aqCampResultAry['data']["aqAdAccount"]) ? $aqCampResultAry['data']["aqAdAccount"] : '';
    $dbaqSector = !empty($aqCampResultAry['data']['aqsector']) ? $aqCampResultAry['data']['aqsector'] : 'Sector not available';
    if(!empty($aqCampResultAry['data']['campaign_id'])){
      $dbCampIdAry = explode(',',$aqCampResultAry['data']['campaign_id']);
    }
  }

  printArray($dbCampIdAry);
  
  $aqMetric = 'all';
  $aqDuration = 'lifetime';
  $aqCampSector = $dbaqSector;
  $aqCity = $dbaqCity;

  // so filter will work with corresponding cities else it will fetch all cities
  $cityBasedSearch = array('real_estate_lessthan_50l','real_estate_50l_1c','real_estate_1c_2c','real_estate_greaterthan_2c');

  // printArray($aqCampSector);
  if(!empty($aqCampSector) && !empty($aqCity)){
    $qryWhere = "";
    if(in_array($aqCampSector,$cityBasedSearch)){
      $qryWhere = " AND  `city` like ? ";
    }else{
      $qryWhere = " AND  (`city` like ? OR `city` like 'ALL') ";
    }
    // Prepare a select statement
    $sql = "SELECT * FROM benchmark_details WHERE `aqsector` LIKE ? $qryWhere ; ";
    if($stmt = mysqli_prepare($mysqlLink, $sql)){

      mysqli_stmt_bind_param($stmt, "ss", $aqCampSector, $aqCity);
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);
            if(mysqli_num_rows($result) >= 1){
                while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                  $benchMarkDetailsData[] = $row;
                }

            } else{
                // URL doesn't contain valid id parameter. Redirect to error page
            }
        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
  }
  if(!empty($benchMarkDetailsData)){
    foreach ($benchMarkDetailsData as $benchMarkData) {
      $benchMarkDetailsAry[$benchMarkData['objective']] = array();
      foreach ($benchMarkData as $key => $value) {
        $benchMarkDetailsAry[$benchMarkData['objective']][$key] = $value;
      }
    }
  }else{
    $errMsg =  '<div class="alert alert-danger">Unable to get data!</div>';
  }
  
  $averageAry = array();
  // Total number of campaigns selected to compare
  $totalCamp = count($dbCampIdAry);
  
  if(!empty($dbCampIdAry)){
    $eachBatch = 6;
    $numberOfBatches = (($totalCamp%$eachBatch) == 0)?(int)($totalCamp/$eachBatch) : (int)($totalCamp/$eachBatch)+1;
    $overAllCamp = array();
    for($i = 0; $i < $numberOfBatches; $i++){
      $currentBatch = array();
      $compareJWith = ($i==0) ? $eachBatch : ( ( ($eachBatch * $i) - 1 ) + $eachBatch);
      for($j = ($i==0) ? 0 : ( ($eachBatch * $i) - 1 ); $j < $compareJWith; $j++)
      {
        if(!$dbCampIdAry[$j])
          break;
        $fbParameters = $dbCampIdAry[$j]."/insights?fields=campaign_name,campaign_id,objective,spend,cpc,ctr,cpp,cpm,cost_per_action_type,cost_per_conversion,cost_per_thruplay&action_attribution_windows=['28d_click','28d_view']&date_preset=".$aqDuration;
        $request = $FB->request('GET', $fbParameters);
        $currentBatch["$j"] = $request;
      }
      try {
        $response = $FB->sendBatchRequest($currentBatch, $dbfb_token);
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }
      
      foreach($response as $data){
        $countOfCampaignRequest++;
        if(!empty($data)){
          $errorCheck = $data->getHeaders();
          $errorCheck = reset(json_decode($errorCheck['X-Business-Use-Case-Usage']))[0];
          if($countOfCampaignRequest == 1 || $countOfCampaignRequest == $totalCamp){
            echo "<script>console.log('Call count = ".$errorCheck->call_count."' + ' Total CPU Time = ".$errorCheck->total_cputime."' + ' Total Time = ".$errorCheck->total_time." Estimated time to regain access = ".$errorCheck->estimated_time_to_regain_access.".');</script>";
          }
          if($errorCheck->total_time > 80 || $errorCheck->total_cputime > 80)
          {
            echo '<div class="alert alert-danger">Facebook Ads Exception: Too many requests have been made from the account. Please try again after one hour.</div>';
            exit;
          }
          $accDataAry[] = $data->getGraphEdge()->asArray();
          }
      }
    }
    if(!empty($accDataAry)){
      $fbDataResultAry = doGetCampDetails1($accDataAry);
      $fbCampNameAry  = doGetFBCampName($accDataAry);
      $sql = "DELETE FROM all_campaign_data WHERE `aqID` = '$aqID'";
      $query = mysqli_query($mysqlLink, $sql);
      foreach($fbDataResultAry as $fbDataResult){
        $json = json_encode($fbDataResult);
        $benchmarkDetailsJson = json_encode($benchMarkDetailsAry);
        $campaignNamesJson = json_encode($fbCampNameAry);
        $sql = "INSERT INTO all_campaign_data (aqID, duration, last_updated, json_data, benchmarks, campaign_names) VALUES('$aqID', '$aqDuration', '$today', '$json', '$benchmarkDetailsJson', '$campaignNamesJson')";
        $query = mysqli_query($mysqlLink, $sql);
      }
    }
  }else{
    //Else matter
  }
  return $fbCampNameAry;
}

// Get fb campaign id(s) from AQ campaign id
function doGetCampIdFromAQId($mysqlLink, $aqID, $clientID){
    $resultAry = array("success"=>false,"msg"=>'Inital error message',"data"=>array());
    $sql = "SELECT campaign_id,platform,aqcampname,aqsector,aqcity,aqinsights,fb_ad_account_name FROM campaigns where id = ? AND client_id = ?";
    if($stmt = mysqli_prepare($mysqlLink, $sql)){
  	    /* bind variables to the prepared statement as parameters */
  	    mysqli_stmt_bind_param($stmt, "ii", $aqID, $clientID);
  	    /* execute query */
  	    mysqli_stmt_execute($stmt);
  	    // echo mysqli_stmt_error($stmt);
  	    mysqli_stmt_store_result($stmt);
  	    if(mysqli_stmt_num_rows($stmt)>0){
  		      /* bind result variables */
  		      mysqli_stmt_bind_result($stmt, $campaign_id, $platform, $aqcampname, $aqsector, $aqcity ,$aqinsights,$aqAdAccount);
  		      /* fetch value */
  		      mysqli_stmt_fetch($stmt);
            $resultAry = array("success"=>true,"msg"=>"","data"=>array('clientID'=>$clientID,'campaign_id'=>$campaign_id, 'platform'=>$platform,'aqcampname'=>$aqcampname,'aqsector'=>$aqsector,'aqcity'=>$aqcity,'aqAdAccount'=>$aqAdAccount,'aqinsights'=>$aqinsights));
  	    }else{
            $resultAry = array("success"=>false,"msg"=>"No data found","data"=>array());
  	    }
    }else{
        $resultAry = array("success"=>false,"msg"=>"Failed to prepare statement","data"=>array());
    }
    return $resultAry;
}

function doGetdetailsFromFBData($dataAry, $metricKey,$optionParam=null){
  $result = null;
  $optionParam=!empty($optionParam) ? $optionParam : 'value';
  if(!empty($dataAry) && !empty($metricKey)){
    $key = array_search($metricKey, array_column($dataAry, 'action_type'));
    $keyValue = $dataAry[$key];
    $result = floatval($keyValue[$optionParam]);
  }
  return $result;
}
function doGetFBCampName($campaignDataArr){
  $fbResultAry = array();
  if(is_array($campaignDataArr) && !empty($campaignDataArr)){
    foreach ($campaignDataArr as $campaignData) {
      // printArray($campaignData);
      $fbCampName = $campaignData[0]['campaign_name'];
      $fbCampId = $campaignData[0]['campaign_id'];
      $fbAry[] = array('campaign_name'=>$fbCampName,'campaign_id'=>$fbCampId);
    }
  }
  $fbResultAry = $fbAry;
  return $fbResultAry;
}
function doGetCPC($dataAry, $obj=null){
  $returnData = '';
  if(!empty($obj)){
    if(!empty($dataAry)){
      if(strtolower($obj) === "app_installs"){
        $returnData = $dataAry[0]['cpc'];
      }else{
        $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_action_type'], 'link_click');
      }
    }
  }
  return $returnData;
}
function doGetCPM($dataAry){
  $returnData = '';
  if(!empty($dataAry)){
    $returnData = !empty($dataAry[0]['cpm']) ? $dataAry[0]['cpm'] : '';
  }
  return $returnData;
}
function doGetCPL($dataAry){
  $returnData = '';
  if(!empty($dataAry[0]['cost_per_action_type'])){
    $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_action_type'], 'leadgen.other');
  }
  return $returnData;
}
function doGetCTR($dataAry){
  $returnData = '';
  if(!empty($dataAry)){
    $returnData = !empty($dataAry[0]['ctr']) ? $dataAry[0]['ctr'] : '';
  }
  return $returnData;
}
function doGetCPV($dataAry){
  $returnData = '';
  if(!empty($dataAry[0]['cost_per_thruplay'])){
    $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_thruplay'], 'video_view');
  }
  return $returnData;
}
function doGetCPTP($dataAry){
  $returnData = '';
  if(!empty($dataAry[0]['cost_per_thruplay'])){
    $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_thruplay'], 'video_view');
  }
  return $returnData;
}
function doGetCPE($dataAry){
  $returnData = '';
  if(!empty($dataAry[0]['cost_per_action_type'])){
    $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_action_type'], 'post_engagement');
  }
  return $returnData;
}
function doGetAppInstall($dataAry){
  // printArray($dataAry);
  $returnData = '';
  $result = !empty($dataAry[0]['cost_per_action_type']) ? $dataAry[0]['cost_per_action_type'] : '';
  if(!empty($result)){
    $returnData = doGetdetailsFromFBData($result, 'mobile_app_install','28d_click');
  }
  return $returnData;
}
function doGetAverageData1($fbDataAry,$param){
  // $key = array_search($metricKey, array_column($dataAry, 'action_type'));
  $result = array();
  $resultAry = array();
  if(is_array($fbDataAry) && !empty($fbDataAry)){
    $objAry = array_unique(array_column($fbDataAry,'objective'));
    foreach($objAry as $obj){
      $cpcAry = $cpmAry = $cplAry = $ctrAry = $cptpAry = $cpvAry = $cpeAry = array();
      foreach($fbDataAry as $fbdata){
        if($fbdata['objective'] == $obj ){
          $cpcAry[] = $fbdata['cpc'];
          $cpmAry[] = $fbdata['cpm'];
          $cplAry[] = $fbdata['cpl'];
          $ctrAry[] = $fbdata['ctr'];
          $cptpAry[] = $fbdata['cptp'];
          $cpvAry[] = $fbdata['cpv'];
          $cpeAry[] = $fbdata['cpe'];
          $cpiAry[] = $fbdata['cpi'];
        }
      }
      $count = count($cpcAry);
      // $cpc_sum = array_sum($cpcAry);
      $cpc_average = !empty($cpcAry) ? (array_sum($cpcAry) / $count) : '';
      $cpm_average = !empty($cpmAry) ? (array_sum($cpmAry) / $count) : '';
      $cpl_average = !empty($cplAry) ? (array_sum($cplAry) / $count) : '';
      $ctr_average = !empty($ctrAry) ? (array_sum($ctrAry) / $count) : '';
      $cptp_average = !empty($cptpAry) ? (array_sum($cptpAry) / $count) : '';
      $cpv_average = !empty($cpvAry) ? (array_sum($cpvAry) / $count) : '';
      $cpe_average = !empty($cpeAry) ? (array_sum($cpeAry) / $count) : '';
      $cpi_average = !empty($cpiAry) ? (array_sum($cpiAry) / $count) : '';

      $resultAry[] = array ('obj'=>$obj, 'cpc'=>$cpc_average, 'cpm'=>$cpm_average, 'cpl'=>$cpl_average, 'ctr'=>$ctr_average, 'cptp'=>$cptp_average, 'cpv'=>$cpv_average, 'cpe'=>$cpe_average, 'cpi'=>$cpi_average);
    }
    // printArray($full_array);
  }
  return $resultAry;
}

function doGetCampDetails1($campaignDataArr){
  $fbResultAry = array();
  if(is_array($campaignDataArr) && !empty($campaignDataArr)){
    // printArray($campaignDataArr);
    foreach ($campaignDataArr as $campaignData) {
      if(!empty($campaignData)){
        $fbObjective = !empty($campaignData[0]['objective']) ? $campaignData[0]['objective'] : '';

        // printArray($campaignData);
        $fbCPC = $fbCPM = $fbCPL = $fbCTR = $fbCPTP = $fbCPV = $fbCPE = '';
        $fbCPC = doGetCPC($campaignData,$fbObjective);
        $fbCPM = doGetCPM($campaignData);
        $fbCPL = doGetCPL($campaignData);
        $fbCTR = doGetCTR($campaignData);
        $fbCPTP = doGetCPTP($campaignData);
        $fbCPV = doGetCPV($campaignData);
        $fbCPE = doGetCPE($campaignData);
        $fbAppInstall = doGetAppInstall($campaignData);
        // $fbCTR = doGetCTR();
        $fbCampName = $campaignData[0]['campaign_name'];
        $fbCampId = $campaignData[0]['campaign_id'];
        $fbAry[] = array('campaign_name'=>$fbCampName,'campaign_id'=>$fbCampId,'objective'=>$fbObjective,'cpc'=>$fbCPC,'cpm'=>$fbCPM,'cpl'=>$fbCPL,'ctr'=>$fbCTR,'cptp'=>$fbCPTP,'cpv'=>$fbCPV,'cpe'=>$fbCPE,'cpi'=>$fbAppInstall);
      }
    }
    // Get average data based on objective
    $fbResultAry = doGetAverageData1($fbAry,'objective');
    // printArray($fbAry);
  }
  return $fbResultAry;
}

function insights($mysqlLink, $campaignData, $metrics){
  $insights = array();
  $categories = array();
  foreach($metrics as $metric){
    if($campaignData[$metric] == 'NA'){
      array_pop($categories);
    } else {
      $sql="SELECT * from insights_codenames where actual_name = '$metric' limit 1";
      $res = mysqli_query($mysqlLink, $sql);
      $row=mysqli_fetch_array($res,MYSQLI_ASSOC);
      $codename = $row['codename'];
      ${$codename} = $campaignData[$metric];
      array_push($categories, $row['category']);
    }
  }
  $categories = array_unique($categories);
  foreach($categories as $category){
    $sql = "SELECT * from insights_formulas where category = '$category'";
    $res = mysqli_query($mysqlLink, $sql);
    $sql_metric = "SELECT * from insights_codenames where category = '$category'";
    $res_metric = mysqli_query($mysqlLink, $sql_metric);
    $actual = mysqli_fetch_array($res_metric,MYSQLI_ASSOC);
    $benchmark = mysqli_fetch_array($res_metric,MYSQLI_ASSOC);

    if($actual['actual_name'] == 'ctr'){
      $diff = number_format((((${$actual['codename']}/${$benchmark['codename']}) -1) * 100), 2, '.','');
    }else{
      $diff = number_format(((1 - (${$actual['codename']}/${$benchmark['codename']})) * 100), 2, '.','');
    }
    if($diff<0){
      $diff = '('.number_format(($diff*-1)).'% decrease compared to benchmark.)';
    }
    else {
      $diff = '('.number_format($diff).'% increase compared to benchmark.)';
    }
    //$diff = number_format($diff).'%';
    //echo $diff;
    if ($res){
      while ($row=mysqli_fetch_array($res,MYSQLI_ASSOC)){
        $formula = !empty($row['formula']) ? $row['formula'] : '';
        $insight = !empty($row['insight']) ? $row['insight'] : 'NA';
        $val = eval("return " . $formula . ";");
        if($val){
          $insights[] = array('metric' => $actual['actual_name'], 'insight' => $insight);
        }
      }
    }
  }
  if(empty($insights))
    return array();
  return $insights;
}
?>